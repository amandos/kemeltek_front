﻿import {DEFAULT,SERVER_DISPATCH,DISPATCH_SUCCESS,DISPATCH_ERROR,SERVER} from '../../constants/';
import axios from 'axios';
import {strings} from '../../constants/lng';
export const stateDefault = ()=> ({
  type: DEFAULT+'SIGNUP',
})
export const serverDispatch = ()=> ({
  type: SERVER_DISPATCH+'SIGNUP',

})
export const dispatchSuccess = (msg)=> ({
  type: DISPATCH_SUCCESS+'SIGNUP',
  msg: msg

})
export const dispatchError = (msg)=> ({
  type: DISPATCH_ERROR+'SIGNUP',
  msg: msg
})
export const signupAction = (values,history)=>{
  return (dispatch,getState)=>{
    dispatch(serverDispatch())
      var querystring = require('querystring');
      axios({
        method: 'post',
        url: SERVER+'/guest/index.php',
        headers: {
          'Content-type': 'application/x-www-form-urlencoded'
        },
        data: querystring.stringify({
          method: 'sign_up',
          name_company: values.name_company,
          name_user: values.name_user,
          user_email: values.user_email,
          login: values.login,
          user_phone: values.phone,
          lang: strings.getLanguage()
        })
      })
      .then(function (response) {
        if (response.data.type === 'error') {
          dispatch(dispatchError(response.data.msg));
        }else if(response.data.type === 'ok'){
          dispatch(dispatchSuccess(response.data.msg));
        }else{
          dispatch(dispatchError('server no response'));
        }
      })
      .catch(function (error) {
        dispatch(dispatchError('server error response'));
      });
  }
}
