﻿import {SERVER_DISPATCH,DISPATCH_SUCCESS,DISPATCH_ERROR,SERVER} from '../../constants/';
import axios from 'axios';
import {strings} from '../../constants/lng';
import {userInfoAction} from '../manage/user_info'
export const serverDispatch = ()=> ({
  type: SERVER_DISPATCH+'SIGNIN_COOKIE',
})
export const dispatchSuccess = (data)=> ({
  type: DISPATCH_SUCCESS+'SIGNIN_COOKIE',
  data: data
})
export const dispatchError = (msg)=> ({
  type: DISPATCH_ERROR+'SIGNIN_COOKIE',
  msg: msg
})
export const signinCookie = (cookie,history)=>{
  return (dispatch,getState)=>{
    dispatch(serverDispatch())
    var querystring = require('querystring');
    var nav = window.navigator;
    var screen = window.screen;
    var guid = nav.mimeTypes.length;
    guid += nav.userAgent.replace(/\D+/g, '');
    guid += nav.plugins.length;
    guid += screen.height || '';
    guid += screen.width || '';
    guid += screen.pixelDepth || '';
    var data = {
      method: 'sign_in_cookies',
      cookie:cookie,
      browser_uniid:guid,
      lang: strings.getLanguage()
    }
    axios({
      method: 'post',
      url: SERVER+'/guest/index.php',
      headers: {
        'Content-type': 'application/x-www-form-urlencoded'
      },
      data: querystring.stringify(data)
    })
    .then(function (response) {
      if (response.data.type === 'error') {
        dispatch(dispatchError());
        localStorage.removeItem('kml_cookie_auth');
        history.push('/guest/signin');
      }else if(response.data.type === 'ok'){
        dispatch(dispatchSuccess('success'));
        sessionStorage.setItem('kml_auth',response.data.authToken);
        dispatch(userInfoAction());
        if(history.location.pathname === '/guest/signin'){
          history.push('/manage/main');
        }

      }else{
        dispatch(dispatchError());
      }
    })
    .catch(function (error) {
      dispatch(dispatchError());
    });
  }
}
