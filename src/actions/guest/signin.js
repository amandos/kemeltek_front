﻿import {DEFAULT,SERVER_DISPATCH,DISPATCH_SUCCESS,DISPATCH_ERROR,SERVER} from '../../constants/';
import axios from 'axios';
import {strings} from '../../constants/lng';
import {reactLocalStorage} from 'reactjs-localstorage';
export const stateDefault = ()=> ({
  type: DEFAULT+'SIGNIN',
})
export const serverDispatch = ()=> ({
  type: SERVER_DISPATCH+'SIGNIN',
})
export const dispatchSuccess = (data)=> ({
  type: DISPATCH_SUCCESS+'SIGNIN',
  data: data
})
export const dispatchError = (msg)=> ({
  type: DISPATCH_ERROR+'SIGNIN',
  msg: msg
})
export const signinAction = (values,history)=>{
  return (dispatch,getState)=>{
    dispatch(serverDispatch())
      var querystring = require('querystring');
      var data;
      if(values.remember){
        var nav = window.navigator;
        var screen = window.screen;
        var guid = nav.mimeTypes.length;
        guid += nav.userAgent.replace(/\D+/g, '');
        guid += nav.plugins.length;
        guid += screen.height || '';
        guid += screen.width || '';
        guid += screen.pixelDepth || '';
        data = {
          method: 'sign_in',
          login: values.login,
          pass: values.pass,
          remember: values.remember,
          browser_uniid:guid,
          lang: strings.getLanguage()
        }
      }else{
        data = {
          method: 'sign_in',
          login: values.login,
          pass: values.pass,
          remember: values.remember,
          lang: strings.getLanguage()
        }
      }
      axios({
        method: 'post',
        url: SERVER+'/guest/index.php',
        headers: {
          'Content-type': 'application/x-www-form-urlencoded'
        },
        data: querystring.stringify(data)
      })
      .then(function (response) {
        if (response.data.type === 'error') {
          dispatch(dispatchError(response.data.msg));
        }else if(response.data.type === 'ok'){
          dispatch(dispatchSuccess('success'));
          sessionStorage.setItem('kml_auth',response.data.authToken);
          if(values.remember){
              reactLocalStorage.set('kml_cookie_auth',response.data.cookie)
          }
          history.push('/manage/main');
        }else{
          dispatch(dispatchError('server no response'));
        }
      })
      .catch(function (error) {
        dispatch(dispatchError('server error response'));
      });
  }
}
