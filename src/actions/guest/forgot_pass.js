﻿import {DEFAULT,SERVER_DISPATCH,DISPATCH_SUCCESS,DISPATCH_ERROR,SERVER} from '../../constants/';
import axios from 'axios';
import {strings} from '../../constants/lng';
export const stateDefault = ()=> ({
  type: DEFAULT+'FORGOT',
})
export const serverDispatch = ()=> ({
  type: SERVER_DISPATCH+'FORGOT',
})
export const dispatchSuccess = (msg)=> ({
  type: DISPATCH_SUCCESS+'FORGOT',
  msg: msg
})
export const dispatchError = (msg)=> ({
  type: DISPATCH_ERROR+'FORGOT',
  msg: msg
})
export const forgotAction = (values)=>{
  return (dispatch,getState)=>{
    dispatch(serverDispatch())
      var querystring = require('querystring');
      axios({
        method: 'post',
        url: SERVER+'/guest/index.php',
        headers: {
          'Content-type': 'application/x-www-form-urlencoded'
        },
        data: querystring.stringify({
          method: 'forgot_password',
          email: values.email,
          lang: strings.getLanguage()
        })
      })
      .then(function (response) {
        if (response.data.type === 'error') {
          dispatch(dispatchError(response.data.msg));
        }else if(response.data.type === 'ok'){
          dispatch(dispatchSuccess(response.data.msg));
        }else{
          dispatch(dispatchError('server no response'));
        }
      })
      .catch(function (error) {
        dispatch(dispatchError('server error response'));
      });
  }
}
