import {DEFAULT,SERVER_DISPATCH,DISPATCH_SUCCESS,DISPATCH_ERROR,SERVER} from '../../constants/';
import axios from 'axios';
import {strings} from '../../constants/lng';
export const stateDefault = ()=> ({
  type: DEFAULT+'WELCOME',
})
export const serverDispatch = ()=> ({
  type: SERVER_DISPATCH+'WELCOME',
})
export const dispatchSuccess = (data)=> ({
  type: DISPATCH_SUCCESS+'WELCOME',
  data: data
})
export const dispatchError = (msg)=> ({
  type: DISPATCH_ERROR+'WELCOME',
  msg: msg
})
export const WelcomeAction = (values,history)=>{
  return (dispatch,getState)=>{
    dispatch(serverDispatch())
      var querystring = require('querystring');
      axios({
        method: 'post',
        url: SERVER+'/guest/index.php',
        headers: {
          'Content-type': 'application/x-www-form-urlencoded'
        },
        data: querystring.stringify({
          method: 'welcome_settings',
          code: values.code,
          lang_user: values.lang,
          pass: values.pass,
          country: values.country,
          city: values.city,
          time_zone: values.time_zone,
          currency: values.currency.name,
          currency_symbol: values.currency.symbol,
          lang: strings.getLanguage()
        })
      })
      .then(function (response) {
        if (response.data.type === 'error') {
          dispatch(dispatchError(response.data.msg));
        }else if(response.data.type === 'ok'){
            dispatch(dispatchSuccess('success'));
            sessionStorage.setItem('kml_auth',response.data.authToken);
            history.push('/manage/main');
        }else{
          dispatch(dispatchError('server no response'));
        }
      })
      .catch(function (error) {
        dispatch(dispatchError('server error response'));
      });
  }
}
