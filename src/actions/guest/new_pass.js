﻿import {DEFAULT,SERVER_DISPATCH,DISPATCH_SUCCESS,DISPATCH_ERROR,SERVER} from '../../constants/';
import axios from 'axios';
import {strings} from '../../constants/lng';
export const stateDefault = ()=> ({
  type: DEFAULT+'NEWPASS',
})
export const serverDispatch = ()=> ({
  type: SERVER_DISPATCH+'NEWPASS',
})
export const dispatchSuccess = (data)=> ({
  type: DISPATCH_SUCCESS+'NEWPASS',
  data: data
})
export const dispatchError = (msg)=> ({
  type: DISPATCH_ERROR+'NEWPASS',
  msg: msg
})
export const newPassAction = (values,history)=>{
  return (dispatch,getState)=>{
    dispatch(serverDispatch())
      var querystring = require('querystring');
      axios({
        method: 'post',
        url: SERVER+'/guest/index.php',
        headers: {
          'Content-type': 'application/x-www-form-urlencoded'
        },
        data: querystring.stringify({
          method: 'new_password',
          code: values.code,
          password: values.password,
          confirm: values.confirm,
          lang: strings.getLanguage()
        })
      })
      .then(function (response) {
        if (response.data.type === 'error') {
          dispatch(dispatchError(response.data.msg));
        }else if(response.data.type === 'ok'){
          dispatch(dispatchSuccess('success'));
            sessionStorage.setItem('kml_auth',response.data.authToken);
            history.push('/manage/main');
        }else{
          dispatch(dispatchError('server no response'));
        }
      })
      .catch(function (error) {
        dispatch(dispatchError('server error response'));
      });
  }
}
