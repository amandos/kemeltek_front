import {SERVER_DISPATCH,DISPATCH_SUCCESS,DISPATCH_ERROR,SERVER} from '../../constants/';
import axios from 'axios';
import {strings} from '../../constants/lng';
export const serverDispatch = ()=> ({
  type: SERVER_DISPATCH+'CHECK_ACTIVATION',
})
export const dispatchSuccess = ()=> ({
  type: DISPATCH_SUCCESS+'CHECK_ACTIVATION',
})
export const dispatchError = (msg)=> ({
  type: DISPATCH_ERROR+'CHECK_ACTIVATION',
  msg: msg
})
export const checkActivation = (code)=>{
  return (dispatch,getState)=>{
    dispatch(serverDispatch())
      var querystring = require('querystring');
      axios({
        method: 'post',
        url: SERVER+'/guest/index.php',
        headers: {
          'Content-type': 'application/x-www-form-urlencoded'
        },
        data: querystring.stringify({
          method: 'check_activation',
          code: code,
          lang: strings.getLanguage()
        })
      })
      .then(function (response) {
        if (response.data.type === 'error') {
          dispatch(dispatchError(response.data.msg));
        }else if(response.data.type === 'ok'){
          dispatch(dispatchSuccess());
        }else{
          dispatch(dispatchError(response));
        }
      })
      .catch(function (error) {
        dispatch(dispatchError(error));
      });
  }
}
