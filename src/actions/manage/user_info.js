import {SERVER_DISPATCH,DISPATCH_SUCCESS,DISPATCH_ERROR,SERVER} from '../../constants/';
import axios from 'axios';
export const serverDispatch = ()=> ({
  type: SERVER_DISPATCH+'USER',
})
export const dispatchSuccess = (user_app)=> ({
  type: DISPATCH_SUCCESS+'USER',
  user_app: user_app
})
export const dispatchError = (msg)=> ({
  type: DISPATCH_ERROR+'USER',
  msg: msg
})
export const userInfoAction = (values,history)=>{
  return (dispatch,getState)=>{
    dispatch(serverDispatch())
      var querystring = require('querystring');
      axios({
        method: 'post',
        url: SERVER+'/manage/index.php',
        headers: {
          'Content-type': 'application/x-www-form-urlencoded'
        },
        data: querystring.stringify({
          method: 'user_info',
          token: sessionStorage.kml_auth
        })
      })
      .then(function (response) {
        if (response.data.type === 'error') {
          dispatch(dispatchError(response.data.msg));
        }else if(response.data.type === 'ok'){
            dispatch(dispatchSuccess(response.data.user_app));
        }else{
          dispatch(dispatchError(response));
        }
      })
      .catch(function (error) {
        dispatch(dispatchError(error));
      });
  }
}
