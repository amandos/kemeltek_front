import {DEFAULT,SERVER_DISPATCH,DISPATCH_SUCCESS,DISPATCH_ERROR,SERVER} from '../../../constants/';
import axios from 'axios';
import {CatListAction} from './category_list'
export const stateDefDelete = ()=> ({
  type: DEFAULT+'CATEGORY_DELETE',
})
export const serverDispatch = ()=> ({
  type: SERVER_DISPATCH+'CATEGORY_DELETE',
})
export const dispatchSuccess = (msg)=> ({
  type: DISPATCH_SUCCESS+'CATEGORY_DELETE',
  msg: msg
})
export const dispatchError = (msg)=> ({
  type: DISPATCH_ERROR+'CATEGORY_DELETE',
  msg: msg
})
export const CatDeleteAction = (cats)=>{
  return (dispatch,getState)=>{
    dispatch(serverDispatch())
      var querystring = require('querystring');
      axios({
        method: 'post',
        url: SERVER+'/manage/goods/index.php',
        headers: {
          'Content-type': 'application/x-www-form-urlencoded'
        },
        data: querystring.stringify({
          method: 'category_status_delete',
          token: sessionStorage.kml_auth,
          cats: cats
        })
      })
      .then(function (response) {
        if (response.data.type === 'error') {
          dispatch(dispatchError(response.data.msg));
        }else if(response.data.type === 'ok'){
          dispatch(dispatchSuccess(response.data.msg));
          dispatch(CatListAction());
        }else{
          dispatch(dispatchError('server no response'));
        }
      })
      .catch(function (error) {
        dispatch(dispatchError('server error response'));
      });
  }
}
