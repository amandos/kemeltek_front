import {DEFAULT,SERVER_DISPATCH,DISPATCH_SUCCESS,DISPATCH_ERROR,SERVER} from '../../../constants/';
import axios from 'axios';
export const stateDefInfo = ()=> ({
  type: DEFAULT+'CATEGORY_INFO',
})
export const serverDispatch = ()=> ({
  type: SERVER_DISPATCH+'CATEGORY_INFO',
})
export const dispatchSuccess = (data,category_list)=> ({
  type: DISPATCH_SUCCESS+'CATEGORY_INFO',
  data: data,
  category_list: category_list
})
export const dispatchError = (msg)=> ({
  type: DISPATCH_ERROR+'CATEGORY_INFO',
  msg: msg
})
export const CatInfoAction = (cat_id)=>{
  return (dispatch,getState)=>{
    dispatch(serverDispatch())
      var querystring = require('querystring');
      axios({
        method: 'post',
        url: SERVER+'/manage/goods/index.php',
        headers: {
          'Content-type': 'application/x-www-form-urlencoded'
        },
        data: querystring.stringify({
          method: 'category_info',
          token: sessionStorage.kml_auth,
          cat_id: cat_id
        })
      })
      .then(function (response) {
        if (response.data.type === 'error') {
          dispatch(dispatchError(response.data.msg));
        }else if(response.data.type === 'ok'){
          dispatch(dispatchSuccess(response.data.data,response.data.category_list));
        }else{
          dispatch(dispatchError('server no response'));
        }
      })
      .catch(function (error) {
        dispatch(dispatchError('server error response'));
      });
  }
}
