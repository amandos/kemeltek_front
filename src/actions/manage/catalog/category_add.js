import {DEFAULT,SERVER_DISPATCH,DISPATCH_SUCCESS,DISPATCH_ERROR,SERVER} from '../../../constants/';
import axios from 'axios';
import {stateDefList} from './category_list'
export const stateDefAdd = ()=> ({
  type: DEFAULT+'CATEGORY_ADD',
})
export const serverDispatch = ()=> ({
  type: SERVER_DISPATCH+'CATEGORY_ADD',
})
export const dispatchSuccess = (msg)=> ({
  type: DISPATCH_SUCCESS+'CATEGORY_ADD',
  msg: msg
})
export const dispatchError = (msg)=> ({
  type: DISPATCH_ERROR+'CATEGORY_ADD',
  msg: msg
})
export const CatAddAction = (values,history)=>{
  return (dispatch,getState)=>{
      dispatch(serverDispatch())
      var querystring = require('querystring');
      axios({
        method: 'post',
        url: SERVER+'/manage/goods/index.php',
        headers: {
          'Content-type': 'application/x-www-form-urlencoded'
        },
        data: querystring.stringify({
          method: 'category_add',
          token: sessionStorage.kml_auth,
          name_cat: values.name_cat,
          parent_cat: values.parent_cat,
          image_cat: values.image_cat,
          color_cat: values.color_cat,
        })
      })
      .then(function (response) {
        if (response.data.type === 'error') {
          dispatch(dispatchError(response.data.msg));
        }else if(response.data.type === 'ok'){
          dispatch(dispatchSuccess(response.data.msg));
          dispatch(stateDefList());
          setTimeout(history.push('/manage/category'), 3000);
        }else{
          dispatch(dispatchError('server no response'));
        }
      })
      .catch(function (error) {
        dispatch(dispatchError('server error response'));
      });
  }
}
