import {DEFAULT,SERVER_DISPATCH,DISPATCH_SUCCESS,DISPATCH_ERROR,SERVER} from '../../../constants/';
import axios from 'axios';
import {CatListAction} from './category_list'
export const stateDefUpdate = ()=> ({
  type: DEFAULT+'CATEGORY_UPDATE',
})
export const serverDispatch = ()=> ({
  type: SERVER_DISPATCH+'CATEGORY_UPDATE',
})
export const dispatchSuccess = (msg)=> ({
  type: DISPATCH_SUCCESS+'CATEGORY_UPDATE',
  msg: msg
})
export const dispatchError = (msg)=> ({
  type: DISPATCH_ERROR+'CATEGORY_UPDATE',
  msg: msg
})
export const CatUpdateAction = (values,history)=>{
  return (dispatch,getState)=>{
    dispatch(serverDispatch())
      var querystring = require('querystring');
      axios({
        method: 'post',
        url: SERVER+'/manage/goods/index.php',
        headers: {
          'Content-type': 'application/x-www-form-urlencoded'
        },
        data: querystring.stringify({
          method: 'category_update',
          token: sessionStorage.kml_auth,
          cat_id: values.cat_id,
          name_cat: values.name_cat,
          parent_cat: values.parent_cat,
          color_cat: values.color_cat,
          image_cat: values.image_cat
        })
      })
      .then(function (response) {
        if (response.data.type === 'error') {
          dispatch(dispatchError(response.data.msg));
        }else if(response.data.type === 'ok'){
          dispatch(dispatchSuccess(response.data.msg));
          dispatch(CatListAction());
          history.push('/manage/category');
        }else{
          dispatch(dispatchError('server no response'));
        }
      })
      .catch(function (error) {
        dispatch(dispatchError('server error response'));
      });
  }
}
