import {DEFAULT,SERVER_DISPATCH,DISPATCH_SUCCESS,DISPATCH_ERROR,SERVER} from '../../../constants/';
import axios from 'axios';
import {GoodsListAction} from './goods_list'
export const stateDefDelete = ()=> ({
  type: DEFAULT+'GOOD_DELETE',
})
export const serverDispatch = ()=> ({
  type: SERVER_DISPATCH+'GOOD_DELETE',
})
export const dispatchSuccess = (msg)=> ({
  type: DISPATCH_SUCCESS+'GOOD_DELETE',
  msg: msg
})
export const dispatchError = (msg)=> ({
  type: DISPATCH_ERROR+'GOOD_DELETE',
  msg: msg
})
export const GoodDeleteAction = (good_id)=>{
  return (dispatch,getState)=>{
    dispatch(serverDispatch())
      var querystring = require('querystring');
      axios({
        method: 'post',
        url: SERVER+'/manage/goods/index.php',
        headers: {
          'Content-type': 'application/x-www-form-urlencoded'
        },
        data: querystring.stringify({
          method: 'good_status_delete',
          token: sessionStorage.kml_auth,
          good_id: good_id
        })
      })
      .then(function (response) {
        if (response.data.type === 'error') {
          dispatch(dispatchError(response.data.msg));
        }else if(response.data.type === 'ok'){
          dispatch(dispatchSuccess(response.data.msg));
          dispatch(GoodsListAction());
        }else{
          dispatch(dispatchError('server no response'));
        }
      })
      .catch(function (error) {
        dispatch(dispatchError('server error response'));
      });
  }
}
