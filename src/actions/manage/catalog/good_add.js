import {DEFAULT,SERVER_DISPATCH,DISPATCH_SUCCESS,DISPATCH_ERROR,SERVER} from '../../../constants/';
import axios from 'axios';
import {stateDefList} from './goods_list'
export const stateDefAdd = ()=> ({
  type: DEFAULT+'GOOD_ADD',
})
export const serverDispatch = ()=> ({
  type: SERVER_DISPATCH+'GOOD_ADD',
})
export const dispatchSuccess = (msg)=> ({
  type: DISPATCH_SUCCESS+'GOOD_ADD',
  msg: msg
})
export const dispatchError = (msg)=> ({
  type: DISPATCH_ERROR+'GOOD_ADD',
  msg: msg
})
export const GoodAddAction = (values,history)=>{
  return (dispatch,getState)=>{
      dispatch(serverDispatch())
      var querystring = require('querystring');
      var data;
      if(parseInt(values.mod_type,10) === 0){
       data = {
          method: 'goods_add',
          token: sessionStorage.kml_auth,
          name: values.name,
          category: values.category,
          mod_type: values.mod_type,
          measure: values.measure,
          color: values.color,
          image: values.image,
          cost_price: values.cost_price,
          price: values.price,
          barcode: values.barcode,
          sku: values.sku,
        }
      }else{
        data = {
          method: 'goods_add',
          token: sessionStorage.kml_auth,
          name: values.name,
          category: values.category,
          mod_type: values.mod_type,
          measure: values.measure,
          color: values.color,
          image: values.image,
          goods: values.goods
        }
      }
      axios({
        method: 'post',
        url: SERVER+'/manage/goods/index.php',
        headers: {
          'Content-type': 'application/x-www-form-urlencoded'
        },
        data: querystring.stringify(data)
      })
      .then(function (response) {
        if (response.data.type === 'error') {
          dispatch(dispatchError(response.data.msg));
        }else if(response.data.type === 'ok'){
          dispatch(dispatchSuccess(response.data.msg));
          dispatch(stateDefList());
          setTimeout(history.push('/manage/goods'), 3000);

        }else{
          dispatch(dispatchError('server no response'));
        }
      })
      .catch(function (error) {
        dispatch(dispatchError('server error response'));
      });
  }
}
