import {DEFAULT,SERVER_DISPATCH,DISPATCH_SUCCESS,DISPATCH_ERROR,SERVER} from '../../../constants/';
import axios from 'axios';
export const stateDefStatus = ()=> ({
  type: DEFAULT+'CATEGORY_STATUS',
})
export const serverDispatch = ()=> ({
  type: SERVER_DISPATCH+'CATEGORY_STATUS',
})
export const dispatchSuccess = (msg)=> ({
  type: DISPATCH_SUCCESS+'CATEGORY_STATUS',
  msg: msg,
})
export const dispatchError = (msg)=> ({
  type: DISPATCH_ERROR+'CATEGORY_STATUS',
  msg: msg
})
export const CatStausAction = (cat_id,status)=>{
  return (dispatch,getState)=>{
    dispatch(serverDispatch())
      var querystring = require('querystring');
      axios({
        method: 'post',
        url: SERVER+'/manage/goods/index.php',
        headers: {
          'Content-type': 'application/x-www-form-urlencoded'
        },
        data: querystring.stringify({
          method: 'category_status_show',
          token: sessionStorage.kml_auth,
          cat_id: cat_id,
          status: status
        })
      })
      .then(function (response) {
        if (response.data.type === 'error') {
          dispatch(dispatchError(response.data.msg));
        }else if(response.data.type === 'ok'){
          dispatch(dispatchSuccess(response.data.msg));
        }else{
          dispatch(dispatchError('server no response'));
        }
      })
      .catch(function (error) {
        dispatch(dispatchError('server error response'));
      });
  }
}
