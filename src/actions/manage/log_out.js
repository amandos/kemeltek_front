import {DEFAULT,SERVER_DISPATCH,DISPATCH_SUCCESS,DISPATCH_ERROR,SERVER} from '../../constants/';
import axios from 'axios';
export const stateDefault = ()=> ({
  type: DEFAULT+'LOGOUT',
})
export const serverDispatch = ()=> ({
  type: SERVER_DISPATCH+'LOGOUT',
})
export const dispatchSuccess = (user_app)=> ({
  type: DISPATCH_SUCCESS+'LOGOUT',
})
export const dispatchError = (msg)=> ({
  type: DISPATCH_ERROR+'LOGOUT',
  msg: msg
})
export const logOutAction = (history)=>{
  return (dispatch,getState)=>{
    dispatch(serverDispatch())
      var querystring = require('querystring');
      var nav = window.navigator;
      var screen = window.screen;
      var guid = nav.mimeTypes.length;
      guid += nav.userAgent.replace(/\D+/g, '');
      guid += nav.plugins.length;
      guid += screen.height || '';
      guid += screen.width || '';
      guid += screen.pixelDepth || '';
      axios({
        method: 'post',
        url: SERVER+'/manage/index.php',
        headers: {
          'Content-type': 'application/x-www-form-urlencoded'
        },
        data: querystring.stringify({
          method: 'remove_cookie',
          browser_uniid: guid,
          token: sessionStorage.kml_auth
        })
      })
      .then(function (response) {
        if (response.data.type === 'error') {
          dispatch(dispatchError(response.data.msg));
        }else if(response.data.type === 'ok'){
            dispatch(dispatchSuccess());
            localStorage.removeItem('kml_cookie_auth');
            sessionStorage.removeItem('kml_auth');
            history.push('/guest/signin');
        }else{
          dispatch(dispatchError('server no response'));
        }
      })
      .catch(function (error) {
        dispatch(dispatchError('server error response'));
      });
  }
}
