import {DEFAULT,SERVER_DISPATCH,DISPATCH_SUCCESS,DISPATCH_ERROR} from '../constants'
const initialState = {
  errorserver: {status: false,msg: false},
  loading: false,
  success: {status: false,data: false},
}
const welcome =(state = initialState, action)=> {
  switch (action.type) {
    case DEFAULT+'WELCOME':
      return {
        ...state,
        errorserver: {status: false,msg: false},
        loading: false,
        success: {status: false,data: false},
      }
    case SERVER_DISPATCH+'WELCOME':
      return {
        ...state,
        errorserver: {status: false,msg: false},
        loading: true,
        success: {status: false,data: false},
      }

    case DISPATCH_SUCCESS+'WELCOME':
      return {
        ...state,
        errorserver: {status: false,msg: false},
        loading: false,
        success: {status: false,data: action.data},
      }

    case DISPATCH_ERROR+'WELCOME':
      return {
        ...state,
        errorserver: {status: true,msg: action.msg},
        loading: false,
        success: {status: false,data: false},
      }
    default:
      return state
  }
}
export default welcome
