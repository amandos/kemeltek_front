import {SERVER_DISPATCH,DISPATCH_SUCCESS,DISPATCH_ERROR} from '../constants'
const initialState = {
  errorserver: {status: false,msg: false},
  success: false,
  loading: false
}
const check_activation =(state = initialState, action)=> {
  switch (action.type) {
    case SERVER_DISPATCH+'CHECK_ACTIVATION':
      return {
        ...state,
        errorserver: {status: false,msg: false},
        success: false,
        loading: true,
      }

    case DISPATCH_SUCCESS+'CHECK_ACTIVATION':
      return {
        ...state,
        errorserver: {status: false,msg: false},
        success: true,
        loading: false,
      }

    case DISPATCH_ERROR+'CHECK_ACTIVATION':
      return {
        ...state,
        errorserver: {status: true,msg: action.msg},
        success: false,
        loading: false,
      }
    default:
      return state
  }
}
export default check_activation
