import {DEFAULT,SERVER_DISPATCH,DISPATCH_SUCCESS,DISPATCH_ERROR} from '../../constants'
const initialState = {
  errorserver: {status: false,msg: false},
  loading: false,
  success: {status: false,data: [],category_list: []},
}
const category_info =(state = initialState, action)=> {
  switch (action.type) {
    case DEFAULT+'CATEGORY_INFO':
      return {
        ...state,
        errorserver: {status: false,msg: false},
        loading: false,
        success: {status: false,data: [],category_list: []},
      }
    case SERVER_DISPATCH+'CATEGORY_INFO':
      return {
        ...state,
        errorserver: {status: false,msg: false},
        loading: true,
        success: {status: false,data: [],category_list: []},
      }

    case DISPATCH_SUCCESS+'CATEGORY_INFO':
      return {
        ...state,
        errorserver: {status: false,msg: false},
        loading: false,
        success: {status: true,data: action.data,category_list: action.category_list},
      }

    case DISPATCH_ERROR+'CATEGORY_INFO':
      return {
        ...state,
        errorserver: {status: true,msg: action.msg},
        loading: false,
        success: {status: false,data: [],category_list: []},

      }
    default:
      return state
  }
}
export default category_info
