import category_list from './category_list'
import category_status from './category_status'
import category_delete from './category_delete'
import category_add from './category_add'
import category_update from './category_update'
import category_info from './category_info'
import goods_list from './goods_list'
import good_status from './good_status'
import good_delete from './good_delete'
import good_add from './good_add'
import { combineReducers} from 'redux'

const catalog = combineReducers({
  category_list,
  category_status,
  category_delete,
  category_add,
  category_update,
  category_info,
  goods_list,
  good_status,
  good_delete,
  good_add
})
export default catalog
