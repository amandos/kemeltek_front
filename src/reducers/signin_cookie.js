import {SERVER_DISPATCH,DISPATCH_SUCCESS,DISPATCH_ERROR} from '../constants'
const initialState = {
  loading: false,
  success: {status: false,data: false},
}
const signin_cookie =(state = initialState, action)=> {
  switch (action.type) {
    case SERVER_DISPATCH+'SIGNIN_COOKIE':
      return {
        ...state,
        loading: true,
        success: {status: false,data: false},
      }

    case DISPATCH_SUCCESS+'SIGNIN_COOKIE':
      return {
        ...state,
        loading: false,
        success: {status: true,data: action.data},
      }

    case DISPATCH_ERROR+'SIGNIN_COOKIE':
      return {
        ...state,
        loading: false,
        success: {status: false,data: false},
      }
    default:
      return state
  }
}
export default signin_cookie
