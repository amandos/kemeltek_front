import {DEFAULT,SERVER_DISPATCH,DISPATCH_SUCCESS,DISPATCH_ERROR} from '../../constants'
const initialState = {
  errorserver: {status: false,msg: false},
  loading: false
}
const log_out =(state = initialState, action)=> {
  switch (action.type) {
    case DEFAULT+'LOGOUT':
      return {
        ...state,
        errorserver: {status: false,msg: false},
        loading: false
      }
    case SERVER_DISPATCH+'LOGOUT':
      return {
        ...state,
        errorserver: {status: false,msg: false},
        loading: true
      }

    case DISPATCH_SUCCESS+'LOGOUT':
      return {
        ...state,
        errorserver: {status: false,msg: false},
        loading: false
      }

    case DISPATCH_ERROR+'LOGOUT':
      return {
        ...state,
        errorserver: {status: true,msg: action.msg},
        loading: false
      }
    default:
      return state
  }
}
export default log_out
