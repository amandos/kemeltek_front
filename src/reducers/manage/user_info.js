import {SERVER_DISPATCH,DISPATCH_SUCCESS,DISPATCH_ERROR} from '../../constants'
const initialState = {
  errorserver: {status: false,msg: false},
  loading: false,
  user_app: []
}
const user_info =(state = initialState, action)=> {
  switch (action.type) {
    case SERVER_DISPATCH+'USER':
      return {
        ...state,
        errorserver: {status: false,msg: false},
        loading: true,
        user_app: []
      }

    case DISPATCH_SUCCESS+'USER':
      return {
        ...state,
        errorserver: {status: false,msg: false},
        loading: false,
        user_app: action.user_app
      }

    case DISPATCH_ERROR+'USER':
      return {
        ...state,
        errorserver: {status: true,msg: action.msg},
        loading: false,
        user_app: []
      }
    default:
      return state
  }
}
export default user_info
