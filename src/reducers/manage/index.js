import user_info from './user_info'
import remove_cookie from './remove_cookie'
import { combineReducers} from 'redux'

const manage = combineReducers({
  user_info,
  remove_cookie
})
export default manage
