import {DEFAULT,SERVER_DISPATCH,DISPATCH_SUCCESS,DISPATCH_ERROR} from '../../constants'
const initialState = {
  errorserver: {status: false,msg: false},
  loading: false,
  success: {status: false,data: false},
}
const new_pass =(state = initialState, action)=> {
  switch (action.type) {
    case DEFAULT+'NEWPASS':
      return {
        ...state,
        errorserver: {status: false,msg: false},
        loading: false,
        success: {status: false,data: false},
      }
    case SERVER_DISPATCH+'NEWPASS':
      return {
        ...state,
        errorserver: {status: false,msg: false},
        loading: true,
        success: {status: false,data: false},
      }

    case DISPATCH_SUCCESS+'NEWPASS':
      return {
        ...state,
        errorserver: {status: false,msg: false},
        loading: false,
        success: {status: true,data: action.data},
      }

    case DISPATCH_ERROR+'NEWPASS':
      return {
        ...state,
        errorserver: {status: true,msg: action.msg},
        loading: false,
        success: {status: false,data: false},
      }
    default:
      return state
  }
}
export default new_pass
