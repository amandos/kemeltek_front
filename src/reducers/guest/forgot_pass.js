import {DEFAULT,SERVER_DISPATCH,DISPATCH_SUCCESS,DISPATCH_ERROR} from '../../constants'
const initialState = {
  errorserver: {status: false,msg: false},
  success: {status: false,msg: false},
  loading: false
}
const forgot_pass =(state = initialState, action)=> {
  switch (action.type) {
    case DEFAULT+'FORGOT':
      return {
        ...state,
        errorserver: {status: false,msg: false},
        loading: false,
        success: {status: false,msg: false},
      }
    case SERVER_DISPATCH+'FORGOT':
      return {
        ...state,
        errorserver: {status: false,msg: false},
        success: {status: false,msg: false},
        loading: true,
      }

    case DISPATCH_SUCCESS+'FORGOT':
      return {
        ...state,
        errorserver: {status: false,msg: false},
        success: {status: true,msg: action.msg},
        loading: false,
      }

    case DISPATCH_ERROR+'FORGOT':
      return {
        ...state,
        errorserver: {status: true,msg: action.msg},
        success: {status: false,msg: false},
        loading: false,
      }
    default:
      return state
  }
}
export default forgot_pass
