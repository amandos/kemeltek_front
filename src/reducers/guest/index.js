import signup from './signup'
import signin from './signin'
import forgot_pass from './forgot_pass'
import new_pass from './new_pass'
import { combineReducers} from 'redux'

const guest = combineReducers({
  signup,
  signin,
  forgot_pass,
  new_pass
})
export default guest
