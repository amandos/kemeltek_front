import { combineReducers} from 'redux'
import { routerReducer } from 'react-router-redux'
import signin_cookie from './signin_cookie'
import welcome from './welcome'
import check_activation from './check_activation'
import manage from './manage'
import catalog from './catalog'
import guest from './guest'
const reducers = combineReducers({
  router: routerReducer,
  signin_cookie:signin_cookie,
  welcome:welcome,
  check_activation:check_activation,
  manage,
  catalog,
  guest
})

export default reducers
