import React from 'react';
import { Provider } from 'react-redux'
import {render} from 'react-dom';
import { Router,Route,Switch,Redirect} from 'react-router-dom'
import createBrowserHistory from 'history/createBrowserHistory'
import configureStore from './store'

import {GuestContainer,} from './containers/guest/'
import {ManageContainer} from './containers/manage/'
import {SignupContainer} from './containers/guest/signup'
import {SigninContainer} from './containers/guest/signin'
import {ForgotContainer} from './containers/guest/forgot_pass'
import {NewpassContainer} from './containers/guest/new_pass'
import MenuList from './components/manage/MenuList'
import {CategoryCont,CategoryAddCont,CategoryUpdateCont} from './containers/manage/catalog'

import {GoodsCont} from './containers/manage/catalog/goods_list'
import {GoodsAddCont} from './containers/manage/catalog/goods_add'
import NoMatch from './components/guest/NoMatch'
import {WelcomeContainer} from './containers/guest/welcome'
import 'antd/dist/antd.css';
import './index.css';

const store = configureStore();
const customHistory = createBrowserHistory();
render(
  <Provider store={store}>
    <Router history={customHistory}>
      <div>
        <Switch>
          <Route exact path='/' component={() => <Redirect to="/guest/signin" />}/>
          <Route path='/guest'>
            <GuestContainer>
              <Route path='/guest/signin' component={SigninContainer} />
              <Route path='/guest/signup' component={SignupContainer} />
              <Route path='/guest/forgot' component={ForgotContainer} />
              <Route path='/guest/forgotpassword/:code' component={NewpassContainer} />
            </GuestContainer>
          </Route>
          <Route path='/welcome/:code' component={WelcomeContainer}/>
          <Route path='/manage'>
            <ManageContainer>
              <Route path='/manage/main' component={MenuList} />
              <Route path='/manage/goods' component={GoodsCont} />
              <Route path='/manage/category' component={CategoryCont} />
              <Route path='/manage/category-add' component={CategoryAddCont} />
              <Route path='/manage/category-edit/:category' component={CategoryUpdateCont} />
              <Route path='/manage/goods-add' component={GoodsAddCont} />
            </ManageContainer>
          </Route>
          <Route component={NoMatch} />
        </Switch>
      </div>
    </Router>
  </Provider>,
  document.getElementById('root')
);
