import React,{Component} from 'react'
import { Layout,Icon,Menu} from 'antd';
import { NavLink } from 'react-router-dom'
import {strings} from '../../constants/lng';
const { Sider } = Layout;
const SubMenu = Menu.SubMenu;
class Navleft extends Component{
  constructor(props){
    super(props)
    this.state = {
      collapsed: false,
    };
  }
  onCollapse = (collapsed) => {
    this.setState({ collapsed });
  }
  render(){
    return(
        <Sider
          collapsible
          collapsed={this.state.collapsed}
          onCollapse={this.onCollapse}
        >
          <Menu theme="dark"
            defaultSelectedKeys={[this.props.keyItem]}
            mode="inline"
            defaultOpenKeys={[this.props.sub]}
            >
            <Menu.Item key="1">
              <Icon type="area-chart" />
              <span>{strings.STATISTICS_NAV}</span>
            </Menu.Item>
            <Menu.Item key="2">
              <Icon type="pay-circle-o" />
              <span>{strings.FINANCE_NAV}</span>
            </Menu.Item>
            <SubMenu
              key="catalog"
              title={<span><Icon type="tags" /><span>{strings.CATALOG_NAV}</span></span>}
            >
                <Menu.Item key="goods">
                  <NavLink to='/manage/goods'>{strings.GOODS_NAV}</NavLink>
                </Menu.Item>
                <Menu.Item key="category">
                  <NavLink to='/manage/category'>{strings.CATEGORY_NAV}</NavLink>
                </Menu.Item>
            </SubMenu>
            <Menu.Item key="10">
              <Icon type="database" />
              <span>{strings.STOCK_NAV}</span>
            </Menu.Item>
            <Menu.Item key="11">
              <Icon type="pie-chart" />
              <span>{strings.MARKETING_NAV}</span>
            </Menu.Item>
            <Menu.Item key="12">
              <Icon type="lock" />
              <span>{strings.ACCESS_NAV}</span>
            </Menu.Item>
            <Menu.Item key="13">
              <Icon type="setting" />
              <span>{strings.SETTINGS_NAV}</span>
            </Menu.Item>
          </Menu>
        </Sider>

    )
  }
}

export default Navleft
