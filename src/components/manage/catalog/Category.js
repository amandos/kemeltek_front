import React,{Component} from 'react'
import { Layout,Form,Popconfirm,Avatar,Card,Table,message,Spin,Input,Row,Icon,Col,Button} from 'antd';
import { NavLink } from 'react-router-dom'
import Navleft from '../Navleft'
import _ from 'lodash'
import {strings} from '../../../constants/lng';
const { Content } = Layout;
const Search = Input.Search;
const FormItem = Form.Item;
var items = [];
class StatusShow extends Component{
  state = {
    value: this.props.value,
    cat_id: this.props.cat_id,
  }
  statusShow = (status,cat_id)=>{
    switch(parseInt(status,10)) {
        case 1:
            status = 0;
            break;
        case 0:
            status = 1;
            break;
        default:
            status = 1
    }
    this.setState({value: status})
    this.props.statusShow(cat_id,status)
  }
  render(){
    const { value, cat_id } = this.state;
    return(
      <a>
        {parseInt(this.state.value,10)===1 ?
            <Icon onClick={()=>this.statusShow(value,cat_id)} type="eye-o" style={{fontSize: '16px'}}/>
          :
            <Icon onClick={()=>this.statusShow(value,cat_id)} type="eye" style={{fontSize: '16px'}}/>}
      </a>
    )
  }
}
class Category extends Component{
  constructor(props){
    super(props)
    this.state = {
      data: [],
      searchData: [],
      searchInput: '',
      categoryList: []
    }
    this.onSearch = this.onSearch.bind(this);
    this.searchTree = this.searchTree.bind(this)
    this.onDelete = this.onDelete.bind(this)
  }
  componentWillMount(){
    if(!this.props.category_success.status){
      this.props.catList();
    }else{
      this.setState({categoryList: this.props.category_success.data})
    }
  }
  onSearch(e){
    this.setState({searchInput: e.target.value});
    var list = _.filter(this.props.category_success.data, function(o) {
      return o.name.toLowerCase().search(
          e.target.value.toLowerCase()
        ) !== -1;
    });

    this.setState({searchData: list});

  }
  componentDidUpdate(){
    if(this.props.errorserver_catstatus.status){
      message.error(this.props.errorserver_catstatus.msg);
      this.props.defaultStatuss();
    }
    if(this.props.errorserver_catdelete.status){
      message.error(this.props.errorserver_catdelete.msg);
      this.props.defaultDelete();
    }
    if(this.props.success_catadd.status){
      message.success(this.props.success_catadd.msg);
      this.props.defaultAdd();
    }

    if(this.props.success_catdelete.status){
      message.success(this.props.success_catdelete.msg);
      this.props.defaultDelete()
    }
    if(this.props.success_catstatus.status){
      message.success(this.props.success_catstatus.msg);
      this.props.defaultStatuss();
    }

    if(this.props.success_catupdate.status){
      message.success(this.props.success_catupdate.msg);
      this.props.defaultUpdate();
    }
  }
  searchTree(record){
    if(record.length>0){
      record.map((item)=>{
        return this.searchTree(item);
      },this)
    }else{
      var item = _.pick(record, ['children']);
      if(item.children){
        items.push(record.id)
        this.searchTree(item.children)
      }else{
        items.push(record.id)
      }
    }
    return items;
  }
  onDelete(record){
    items = [];
    var children = this.searchTree(record);
    this.props.catDelete(_.join(children,[',']));
  }
  componentWillReceiveProps(props){
    if(props.category_success.status){
      this.setState({categoryList: props.category_success.data})
    }
  }
  render(){
    var display = window.innerHeight;
    var contentHeight = (display*90)/100;
    const locale = {
      filterTitle: strings.FILTER,
      filterConfirm: strings.FILTER,
      filterReset: strings.FILTER_CONFIRM,
      emptyText: strings.EMPTY_DATA,
    };
    const columns = [
      {
        title: strings.CATEGORY,
        dataIndex: 'name',
        key: 'name',
        width: '85%',
        sorter: (a, b) => a.name.length - b.name.length,
        render: (name, record) => (
         <span>
            {
              record.image.length>0 ?
                  <img style={{width: '35px',height: 'auto'}} src={record.image} alt={name}/>
                :
                  record.color === '#ffffff' ?
                    <img style={{width: '35px',height: 'auto'}} src='/images/item-photo.png' alt={name}/>
                  :
                    <Avatar style={{ backgroundColor: record.color, verticalAlign: 'middle' }} shape='square' size="large">
                      {name.slice(0, 2).toUpperCase()}
                    </Avatar>
            }

            &nbsp;{name}
         </span>
        ),
      },
      {
        title: '',
        dataIndex: 'key',
        key: 'edit',
        width: '5%',
        render: id => <NavLink to={"/manage/category-edit/"+id}><Icon style={{fontSize: '16px'}} type="edit" /></NavLink>,
      },
      {
        title: '',
        dataIndex: 'status_show',
        key: 'status_show',
        width: '5%',
        render: (status, record) =>  (
          <StatusShow statusShow={this.props.catStatus} value={status} cat_id={record.id}/>
        ),
      },
      {
        title: '',
        dataIndex: 'id',
        key: 'delete',
        width: '5%',
        render: (text, record) =>
        (
            <Popconfirm title={strings.CONFIRMATION_DELETE_CATEGORY_TEXT} okText={strings.OK_TEXT} cancelText={strings.CANCEL} onConfirm={() => this.onDelete(record)}>
              <a><Icon type="delete" style={{fontSize: '16px'}}/></a>
            </Popconfirm>
        )
      }
    ];
    return(
      <Layout>
        <Navleft  keyItem="category" sub="catalog"/>
        <Content style={{minHeight: contentHeight}}>
          <Card  className='titleHead'>
            <Row type="flex" align="middle">
              <Col span={20}>
                <h2>{strings.CATEGORY}</h2>
              </Col>
              <Col span={4}>
                <NavLink to='/manage/category-add'><Button style={{width: '95%'}} type="primary">{strings.CATEGORY_ADD_NAV}</Button></NavLink>
              </Col>
            </Row>
          </Card>
          <Spin spinning={this.props.loading_catlist || this.props.loading_catstatus || this.props.loading_catdelete}>
            {
              this.props.errorserver_catlist.status ?
                <Row type="flex" justify="center" align="middle">
                  <Col style={{padding: '20px 0'}} span={8}>
                    <Card style={{textAlign: 'center'}}>
                      <h4>{this.props.errorserver_catlist.msg}</h4>
                    </Card>
                  </Col>
                </Row>
              :
                <div>
                  <Card  type="inner" bordered={false}>
                    <Row>
                      <Col span={12}>
                        <FormItem label={strings.SEARCH_CATEGORY_TEXT}  >
                           <Search
                             onChange={this.onSearch}
                             style={{ width: '100%' }}
                             disabled={!this.props.category_success.status}
                             placeholder={strings.SEARCH_CATEGORY_PLACEHOLDER}
                           />
                        </FormItem>
                      </Col>
                    </Row>
                  </Card>
                  <Table
                    locale={locale}
                    columns={columns}
                    pagination={{position: "top",total: this.state.searchInput.length > 0 ? this.state.searchData.length :  this.state.categoryList.length,pageSize:4}}
                    dataSource={this.state.searchInput.length > 0 ? this.state.searchData : this.state.categoryList} />
                </div>
            }
          </Spin>
        </Content>
      </Layout>
    )
  }
}
export default Category
