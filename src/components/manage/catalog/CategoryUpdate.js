import React,{Component} from 'react'
import { Layout,Form,Card,Upload,Spin,Affix,message,TreeSelect,Input,Button,Icon,Row,Col} from 'antd';
import Navleft from '../Navleft'
import {SERVER} from '../../../constants/';
import { CirclePicker } from 'react-color'
import {strings} from '../../../constants/lng';
import { NavLink } from 'react-router-dom'
const { Content } = Layout;
const FormItem = Form.Item;
class CategoryUpdate extends Component{
  constructor(props){
    super(props);
    this.state = {
      categoryValue: strings.MAIN_CATEGORY_TEXT,
      categoryId: "0",
      background: '#eeeeee',
      image_origin: '',
      image: '',
      name_image:'',
      name: '',
      categoryList: []
    }
    this.imageChange = this.imageChange.bind(this);
    this.chageParent = this.chageParent.bind(this);
  }
  handleChangeComplete = (color) => {
    this.setState({ background: color.hex });
  };
  beforeUpload(file) {
    const isJPG = file.type === 'image/jpeg';
    if (!isJPG) {
      message.error('You can only upload JPG file!');
    }
    const isLt2M = file.size / 1024 / 1024 < 2;
    if (!isLt2M) {
      message.error('Image must smaller than 2MB!');
    }
    return isJPG && isLt2M;
  }
  imageChange(info){
    const reader = new FileReader();
    reader.readAsDataURL(info.file.originFileObj);
    reader.addEventListener('load', () =>this.setState({
        image: reader.result,
        name_image: info.file.originFileObj.name,
      })
    );
  }
  componentWillMount(){
    this.props.catInfo(this.props.match.params.category)
  }
  handleSubmit=(e)=>{
    e.preventDefault();
    this.props.form.validateFields(['name_cat','parent_cat','image_cat','color_cat'],(err, values,) => {
      if (!err) {
        values.cat_id = this.props.match.params.category;
        if(this.state.background==='#eeeeee'){
            values.color_cat = '#ffffff';
        }else{
          values.color_cat = this.state.background;
        }
        if(this.state.image===this.state.image_origin){
          values.image_cat = false;
        }else{
          values.image_cat = this.state.image;
        }
        values.parent_cat = this.state.categoryId;
        this.props.catUpdate(values,this.props.history);
      }
    })
  }
  componentDidUpdate(){
    if(this.props.errorserver_catupdate.status){
      message.error(this.props.errorserver_catupdate.msg);
      this.props.defaultUpdate()
    }
  }
  chageParent(value,e,extra){
    if(extra.triggerNode){
      this.setState({categoryValue: value});
      this.setState({categoryId: extra.triggerNode.props.id})
    }
  }
  componentWillReceiveProps(props){
    if(props.success_catinfo.status){
      this.setState({image: props.success_catinfo.data.image});
      this.setState({image_origin: props.success_catinfo.data.image});
      this.setState({name: props.success_catinfo.data.name});
      if(props.success_catinfo.data.color!=='#ffffff'){
        this.setState({background: props.success_catinfo.data.color})
      }
      if(parseInt(props.success_catinfo.data.parent_category,10)!==0){
        this.setState({categoryValue: props.success_catinfo.data.parent_category_value});
        this.setState({categoryId: props.success_catinfo.data.parent_category_id});
      }else{
        this.setState({categoryValue: strings.MAIN_CATEGORY_TEXT});
        this.setState({categoryId: "0"});
      }
      this.setState({categoryList: props.success_catinfo.category_list});
      this.props.defaultInfo();
    }
  }
  render(){
    var display = window.innerHeight;
    var contentHeight = (display*90)/100;
    const { getFieldDecorator} = this.props.form;
    return(
      <Layout>
        <Navleft keyItem="category" sub="catalog"/>
        <Content  style={{minHeight: contentHeight}}>
          <Spin spinning={this.props.loading_catupdate || this.props.loading_catinfo}>
            {
              this.props.errorserver_catinfo.status ?
                <Row type="flex" justify="center" align="middle">
                  <Col style={{padding: '20px 0'}} span={8}>
                    <Card style={{textAlign: 'center'}}>
                      <h4>{this.props.errorserver_catinfo.msg}</h4>
                    </Card>
                  </Col>
                </Row>
              :
                <Form layout='vertical' onSubmit={this.handleSubmit}>
                  <Affix>
                    <Card  className='titleHead'>
                      <Row type="flex" align="middle">
                        <Col span={21}>
                          <h2><NavLink to='/manage/category'><Icon className="pref-link" type="arrow-left" /></NavLink>Изменить категорию</h2>
                        </Col>
                        <Col span={3}>
                           <Button size='large' htmlType="submit" type="primary">Изменить</Button>
                        </Col>
                      </Row>
                    </Card>
                  </Affix>
                  <Card style={{minHeight: contentHeight}}>
                    <Row>
                      <Col span={10}>
                        <FormItem label={strings.NAME_INPUT_CATEGORY_LABEL}>
                           {getFieldDecorator('name_cat', {
                             rules: [{
                               required: true, message: strings.REQUIRE_INPUT_NAME_CATEGORY,
                             }],
                             initialValue: this.state.name,
                           })(
                             <Input />
                           )}
                        </FormItem>
                      </Col>
                      <Col offset={1} span={10}>
                        <FormItem label={strings.PARENT_CATEGORY_INPUT_ADDCATEGORY}>
                           {getFieldDecorator('parent_cat',{
                             initialValue: this.state.categoryValue
                           })(
                              <TreeSelect
                                 showSearch
                                 dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
                                 treeData={this.state.categoryList}
                                 onChange={this.chageParent}
                                 treeDefaultExpandAll
                              />
                           )}
                        </FormItem>
                      </Col>
                    </Row>
                    <Row>
                      <Col span={10}>
                        <FormItem label={strings.IMAGE_INPUT}>
                              <Upload
                                name="avatar"
                                action={SERVER}
                                beforeUpload={this.beforeUpload}
                                showUploadList={false}
                                onChange={this.imageChange}
                              >
                                <Button>
                                  <Icon type='upload' />
                                  {this.state.image.length > 0 ?
                                    strings.EDIT_TEXT
                                  :
                                    strings.UPLOAD_TEXT}
                                </Button>
                                <br />
                                {
                                  this.state.image.length > 0 ?
                                    <Card style={{marginTop: '5px'}}>
                                      <img style={{width: '65px',height: 'auto'}} src={this.state.image} alt={this.state.name_image}/>
                                    </Card>
                                  :
                                    false
                                }
                              </Upload>
                        </FormItem>
                      </Col>
                      <Col offset={1} span={10}>
                        <FormItem label={strings.COLOR_TEXT}>
                           {getFieldDecorator('color_cat')(
                                <CirclePicker width={'100%'}
                                color={ this.state.background }
                                circleSize={25} circleSpacing={10}
                                onChangeComplete={ this.handleChangeComplete }
                                colors={["#eeeeee","#b71c1c", "#ff5722", "#ffeb3b", "#4caf50", "#2196f3", "#3f51b5", "#9c27b0", "#795548", "#009688", "#8bc34a", "#fa28ff"]} />
                           )}
                         </FormItem>
                      </Col>
                    </Row>
                  </Card>
                </Form>
            }
          </Spin>
        </Content>
      </Layout>
    )
  }
}
CategoryUpdate = Form.create()(CategoryUpdate);
export default CategoryUpdate
