import React,{Component} from 'react'
import { Layout,Form,Radio,Upload,Spin,Divider,Affix,message,Card,Checkbox,TreeSelect,Input,Button,Icon,Row,Col} from 'antd';
import Navleft from '../Navleft'
import {SERVER} from '../../../constants/';
import VariantsProduct from './VariantsProduct'
import StandardProduct from './StandardProduct'
import {strings} from '../../../constants/lng';
import { CirclePicker } from 'react-color'
const { Content } = Layout;
const FormItem = Form.Item;
const RadioButton = Radio.Button;
const RadioGroup = Radio.Group;
class GoodsAdd extends Component{
  constructor(props){
    super(props);
    this.state = {
      categoryValue: strings.MAIN_CATEGORY_TEXT,
      categoryId: "0",
      background: '#eeeeee',
      image: '',
      name_image:'',
      categoryList: []
    }
    this.imageChange = this.imageChange.bind(this);
    this.chageParent = this.chageParent.bind(this);
    this.modGoods = this.modGoods.bind(this);
  }
  componentWillMount(){
    if(!this.props.category_success.status){
      this.props.catList()
    }else{
      var data =  this.props.category_success.data.slice()
      data.unshift({key: "0",label: strings.MAIN_CATEGORY_TEXT,value: strings.MAIN_CATEGORY_TEXT,id: "0"})
      this.setState({categoryList: data})
    }
  }
  componentDidUpdate(){
    if(this.props.errorserver_goodadd.status){
      message.error(this.props.errorserver_goodadd.msg);
      this.props.defaultAdd()
    }
  }
  modGoods(i){
    var goods;
    this.props.form.validateFields([`nameMod-${i}`,`barcodeMod-${i}`,`skuMod-${i}`,`costPriceMod-${i}`,`priceMod-${i}`],(err,values)=>{
      if(!err){
        goods =  {
          name: values[`nameMod-${i}`],
          barcode: values[`barcodeMod-${i}`],
          sku: values[`skuMod-${i}`],
          cost_price: values[`costPriceMod-${i}`],
          price: values[`priceMod-${i}`]
        };
      }else{
        goods = false;
      }
    });
    return goods;
  }
  handleSubmit=(e)=>{
    e.preventDefault();
    if(this.props.form.getFieldValue('mod_type') === "0"){
      this.props.form.validateFields(['name','measure','mod_type','barcode','sku','cost_price','price'],(err, values,) => {
        if (!err) {
          if(this.state.background==='#eeeeee'){
              values.color = '#ffffff';
          }else{
            values.color = this.state.background;
          }
          values.image = this.state.image;
          values.category = this.state.categoryId;
          if(values.measure){
            values.measure = 1;
          }else{
            values.measure = 0;
          }
          this.props.goodsAdd(values,this.props.history);
        }
      })
    }else{
      this.props.form.validateFields(['name','measure','mod_type'],(err, values,) => {
        if (!err) {
          if(this.state.background==='#eeeeee'){
              values.color = '#ffffff';
          }else{
            values.color = this.state.background;
          }
          values.image = this.state.image;
          values.category = this.state.categoryId;
          if(values.measure){
            values.measure = 1;
          }else{
            values.measure = 0;
          }
          const goods = [];
          const keys = this.props.form.getFieldValue('keys');
          const errMod = [];
          for(var i=0; i<=keys.length; i++){
            var mod = this.modGoods(i);
            if(mod !== false){
              goods.push(
                  mod
              )
            }else{
              errMod.push(mod);
            }
          }
          if(errMod.length === 0){
            values.goods = JSON.stringify(goods);
            this.props.goodsAdd(values,this.props.history);
          }
        }
      });
    }
  }
  componentWillReceiveProps(props){
    if(props.category_success.status){
      var data =  props.category_success.data.slice()
      data.unshift({key: "0",label: strings.MAIN_CATEGORY_TEXT,value: strings.MAIN_CATEGORY_TEXT,id: "0"})
      this.setState({categoryList: data})
    }
  }
  chageParent(value,e,extra){
    if(extra.triggerNode){
      this.setState({categoryValue: value});
      this.setState({categoryId: extra.triggerNode.props.id})
    }
  }
  handleChangeComplete = (color) => {
    this.setState({ background: color.hex });
  };
  beforeUpload(file) {
    const isJPG = file.type === 'image/jpeg';
    if (!isJPG) {
      message.error('You can only upload JPG file!');
    }
    const isLt2M = file.size / 1024 / 1024 < 2;
    if (!isLt2M) {
      message.error('Image must smaller than 2MB!');
    }
    return isJPG && isLt2M;
  }
  imageChange(info){
    const reader = new FileReader();
    reader.readAsDataURL(info.file.originFileObj);
    reader.addEventListener('load', () =>this.setState({
        image: reader.result,
        name_image: info.file.originFileObj.name,
      })
    );
  }
  render(){
    var display = window.innerHeight;
    var contentHeight = (display*90)/100;
    const { getFieldDecorator,getFieldValue} = this.props.form;
    return(
      <Layout>
        <Navleft  keyItem="goods" sub="catalog"/>
        <Content style={{minHeight: contentHeight}}>
          <Spin spinning={this.props.loading_goodadd}>
            <Form layout='vertical' onSubmit={this.handleSubmit}>
              <Affix>
                <Card  className='titleHead'>
                  <Row type="flex" align="middle">
                    <Col span={21}>
                      <h2><Icon style={{color: "#becbcf"}} type="arrow-left" />Новый товар</h2>
                    </Col>
                    <Col span={3}>
                       <Button size='large' htmlType="submit" type="primary">Сохранить</Button>
                    </Col>
                  </Row>
                </Card>
              </Affix>
              <Card>
              <h2>Главный</h2>
                <Row>
                  <Col span={10}>
                    <FormItem
                       label="Название"
                     >
                       {getFieldDecorator('name', {
                         rules: [{
                           required: true, message: 'Поле «Название» обязательно для заполнения.',
                         }],
                       })(
                         <Input />
                       )}
                    </FormItem>
                  </Col>
                  <Col offset={1} span={10}>
                    <FormItem label={strings.PARENT_CATEGORY_INPUT_ADDCATEGORY}>
                       {getFieldDecorator('category',{
                         initialValue: this.state.categoryValue ? this.state.categoryValue : strings.MAIN_CATEGORY_TEXT
                       })(
                          <TreeSelect
                             showSearch
                             dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
                             treeData={this.state.categoryList}
                             onChange={this.chageParent}
                             treeDefaultExpandAll
                          />
                       )}
                    </FormItem>
                  </Col>
                </Row>
                <Row>
                  <Col span={10}>
                    <FormItem label="Опции" >
                        <FormItem>
                          {getFieldDecorator('measure')(
                            <Checkbox>Весовой товар (цена за 100 г)</Checkbox>
                          )}
                        </FormItem>
                    </FormItem>
                  </Col>
                  <Col offset={1} span={10}>
                    <FormItem label={strings.COLOR_TEXT}>
                       {getFieldDecorator('color')(
                            <CirclePicker width={'100%'}
                            color={ this.state.background }
                            circleSize={25} circleSpacing={10}
                            onChangeComplete={ this.handleChangeComplete }
                            colors={["#eeeeee","#000000","#b71c1c", "#ff5722", "#ffeb3b", "#4caf50", "#2196f3", "#3f51b5", "#9c27b0", "#795548", "#009688", "#fa28ff"]} />
                       )}
                     </FormItem>
                  </Col>
                </Row>
                <Row>
                  <Col span={10}>
                    <FormItem label={strings.IMAGE_INPUT}>
                          <Upload
                            name="avatar"
                            action={SERVER}
                            beforeUpload={this.beforeUpload}
                            showUploadList={false}
                            onChange={this.imageChange}
                          >
                            <Button>
                              <Icon type='upload' />
                              {this.state.image.length > 0 ?
                                strings.EDIT_TEXT
                              :
                                strings.UPLOAD_TEXT}
                            </Button>
                            <br />
                            {
                              this.state.image.length > 0 ?
                                <Card style={{marginTop: '5px'}}>
                                  <img style={{width: '65px',height: 'auto'}} src={this.state.image} alt={this.state.name_image}/>
                                </Card>
                              :
                                false
                            }
                          </Upload>
                    </FormItem>
                  </Col>
                  <Col offset={1} span={10}>
                    <br />
                    <FormItem label="">
                      {getFieldDecorator('mod_type', {
                        initialValue: "0"
                      })(
                        <RadioGroup>
                          <RadioButton value="0">Один вид товара</RadioButton>
                          <RadioButton value="1">Несколько видов товара</RadioButton>
                        </RadioGroup>
                      )}

                    </FormItem>
                  </Col>
                </Row>
                <Divider />
                <h2>Цена и штрих-код</h2>
                {
                  getFieldValue('mod_type') === "0" ?
                    <StandardProduct form={this.props.form} />
                  :
                    <VariantsProduct form={this.props.form} />
                }
              </Card>
            </Form>
          </Spin>
        </Content>
      </Layout>
    )
  }
}
GoodsAdd = Form.create()(GoodsAdd);
export default GoodsAdd
