import React,{Component} from 'react'
import { Layout,Form,Spin,Card,Input,message,Table,Popconfirm,Avatar,Button,TreeSelect,Icon,Row,Col} from 'antd';
import Navleft from '../Navleft'
import { NavLink } from 'react-router-dom'
import _ from 'lodash'
import {strings} from '../../../constants/lng';
const { Content } = Layout;
const Search = Input.Search;
const FormItem = Form.Item;
class StatusShow extends Component{
  state = {
    value: this.props.value,
    good_id: this.props.good_id,
  }
  statusShow = (status,good_id)=>{
    switch(parseInt(status,10)) {
        case 1:
            status = 0;
            break;
        case 0:
            status = 1;
            break;
        default:
            status = 1
    }
    this.setState({value: status})
    this.props.statusShow(good_id,status)
  }
  render(){
    const { value, good_id } = this.state;
    return(
      <a>
        {parseInt(this.state.value,10)===1 ?
            <Icon onClick={()=>this.statusShow(value,good_id)} type="eye-o" style={{fontSize: '16px'}}/>
          :
            <Icon onClick={()=>this.statusShow(value,good_id)} type="eye" style={{fontSize: '16px'}}/>}
      </a>
    )
  }
}
class Goods extends Component{
  constructor(props){
    super(props)
    this.state = {
      searchInput:'',
      searchData: [],
      categoryList: [],
      categoryValue: strings.ALL_CATEGORY_LABEL,
      data: []
    }
    this.onSearch = this.onSearch.bind(this);
    this.categoryFiler = this.categoryFiler.bind(this);
  }
  componentWillMount(){
    if(!this.props.goods_success.status){
        this.props.goodsList();
    }else{
      this.setState({data: this.props.goods_success.data});
    }
    if(!this.props.category_success.status){
      this.props.catList()
    }else{
      var data =  this.props.category_success.data.slice()
      data.unshift({key: "0",label: strings.MAIN_CATEGORY_TEXT,value: strings.MAIN_CATEGORY_TEXT,id: "0"})
      data.unshift({key: "all",label: strings.ALL_CATEGORY_LABEL,value: strings.ALL_CATEGORY_LABEL,id: "all"})
      this.setState({categoryList: data})
    }
  }
  componentWillReceiveProps(props){
    if(props.goods_success.status){
        this.setState({data: props.goods_success.data});
    }
    if(props.category_success.status){
      var data =  props.category_success.data.slice();
      data.unshift({key: "0",label: strings.MAIN_CATEGORY_TEXT,value: strings.MAIN_CATEGORY_TEXT,id: "0"})
      data.unshift({key: "all",label: strings.ALL_CATEGORY_LABEL,value: strings.ALL_CATEGORY_LABEL,id: "all"})
      this.setState({categoryList: data})
    }
  }
  componentDidUpdate(){
    if(this.props.errorserver_goodstatus.status){
      message.error(this.props.errorserver_goodstatus.msg);
      this.props.defaultStatuss();
    }
    if(this.props.success_goodstatus.status){
      message.success(this.props.success_goodstatus.msg);
      this.props.defaultStatuss();
    }
    if(this.props.errorserver_gooddelete.status){
      message.error(this.props.errorserver_gooddelete.msg);
      this.props.defaultDelete();
    }
    if(this.props.success_gooddelete.status){
      message.success(this.props.success_gooddelete.msg);
      this.props.defaultDelete()
    }
    if(this.props.success_goodadd.status){
      message.success(this.props.success_goodadd.msg);
      this.props.defaultAdd();
    }

  }
  onDelete(good_id){
    this.props.goodsDelete(good_id);
  }
  onSearch(e){
    this.setState({searchInput: e.target.value});
    var list = _.filter(this.state.data, function(o) {
       if(o.goods_name.toLowerCase().search(
          e.target.value.toLowerCase()
        ) !== -1){
          return o;
        };
        if(o.barcode === e.target.value){
          return o;
        }
        if(o.sku === e.target.value){
          return o;
        }
        if(o.children){
          var c = _.filter(o.children, function(c) {
             if(c.goods_name.toLowerCase().search(
                e.target.value.toLowerCase()
              ) !== -1){
                return c;
              };
              if(c.barcode === e.target.value){
                return c;
              }
              if(c.sku === e.target.value){
                return c;
              }
           },e);
           if(c.length>0){
              return c;
           }
        };
    });

    this.setState({searchData: list});

  }
  categoryFiler(value,e,extra){
    if(extra.triggerNode){
      this.setState({categoryValue: value});
      if(extra.triggerNode.props.id !== 'all'){
        var data = _.filter(this.state.data, function(o) {
         if(parseInt(o.cat_id,10) === parseInt(extra.triggerNode.props.id,10)){
           return o;
         }
       },extra)
       this.setState({searchData: data})
      }
    }
  }
  render(){
    var display = window.innerHeight;
    var contentHeight = (display*90)/100;
    const locale = {
      filterTitle: strings.FILTER,
      filterConfirm: strings.FILTER,
      filterReset: strings.FILTER_CONFIRM,
      emptyText: strings.EMPTY_DATA,
    };
    const columns = [
      {
        title: strings.GOODS,
        dataIndex: 'goods_name',
        key: 'goods_name',
        render: (goods_name, record) => (
       <span>
          {
            record.image !== "" && record.image !== null  ?
                <img style={{width: '35px',height: 'auto'}} src={record.image} alt={goods_name}/>
              :
                record.color === '#ffffff' ?
                  <img style={{width: '35px',height: 'auto'}} src='/images/item-photo.png' alt={goods_name}/>
                :
                  <Avatar style={{ backgroundColor: record.color, verticalAlign: 'middle' }} shape='square' size="large">
                    {goods_name.slice(0, 2).toUpperCase()}
                  </Avatar>
          }
          &nbsp;{goods_name}
       </span>
      ),
      },
      {
        title: strings.CATEGORY,
        dataIndex: 'cat_name',
        key: 'cat_name',
        render: (cat_name)=>(
          cat_name === null ? strings.MAIN_CATEGORY_TEXT : cat_name
        )
      },
      {
        title: strings.SKU,
        dataIndex: 'sku',
        key: 'sku',
        render: (sku) => (
            <span>{sku !== null && sku !== ''  ? sku : '-' }</span>
        )
      },
      {
        title: strings.BARCODE,
        dataIndex: 'barcode',
        key: 'barcode',
        render: (barcode) => (
            <span>{barcode !== null && barcode !== ''  ? barcode : '-' }</span>
        )
      },
      {
        title: strings.COST_PRICE_TABLE,
        dataIndex: 'cost_price',
        key: 'cost_price',
        render: (cost_price) => (
            <span>{cost_price !== null && cost_price !== ''  ? cost_price : '-' }</span>
        )
      },
      {
        title: strings.PRICE,
        dataIndex: 'price',
        key: 'price',
        render: (price) => (
            <span>{price !== null && price !== ''  ? price : '-' }</span>
        )
      },
      {
        title: strings.MARKUP,
        dataIndex: 'markup',
        key: 'markup',
        render: (markup)=>(
          <span>{markup !== null && markup !== ''  ? parseInt(markup,10)+' %' : '-' }</span>
        )
      },
      {
        title: '',
        dataIndex: 'key',
        key: 'edit',
        width: '5%',
        render: (goods_id,record) =>(
          record.mod_id === '0' ?
            <NavLink to={"/manage/goods-edit/"+goods_id}><Icon style={{fontSize: '16px'}} type="edit" /></NavLink>

          :
            false
        )
      },
      {
        title: '',
        dataIndex: 'status_show',
        key: 'status_show',
        width: '5%',
        render: (status, record) =>  (
          record.mod_id === '0' ?
            <StatusShow statusShow={this.props.goodStatus} value={status} good_id={record.goods_id}/>
          :
            false
        ),
      },
      {
        title: '',
        dataIndex: 'key',
        key: 'delete',
        width: '5%',
        render: (goods_id, record) =>
        (
          record.mod_id === '0' ?
            <Popconfirm title={strings.CONFIRMATION_DELETE_GOODS_TEXT} okText={strings.OK_TEXT} cancelText={strings.CANCEL} onConfirm={() => this.onDelete(goods_id)}>
              <a><Icon type="delete" style={{fontSize: '16px'}}/></a>
            </Popconfirm>
          :
            false
        )
      }
    ];
    return(
      <Layout>
        <Navleft keyItem="goods" sub="catalog"/>
        <Content style={{minHeight: contentHeight}}>
            <Card  className='titleHead'>
              <Row type="flex" align="middle">
                <Col span={15}>
                  <h2>{strings.GOODS_NAV}</h2>
                </Col>
                <Col span={3}>
                  <Button style={{width: '95%'}} icon="export" type="dashed">{strings.EXPORT_BTN}</Button>
                </Col>
                <Col span={3}>
                  <Button style={{width: '95%'}} icon="upload" type="dashed">{strings.IMPORT_BTN}</Button>
                </Col>
                <Col span={3}>
                  <NavLink to='/manage/goods-add'><Button style={{width: '95%'}} type="primary">{strings.GOOD_ADD_NAV}</Button></NavLink>
                </Col>
              </Row>
            </Card>
            <Spin spinning={this.props.loading_goodslist || this.props.loading_goodstatus || this.props.loading_gooddelete}>
              {
                this.props.errorserver_goodslist.status ?
                  <Row type="flex" justify="center" align="middle">
                    <Col style={{padding: '20px 0'}} span={8}>
                      <Card style={{textAlign: 'center'}}>
                        <h4>{this.props.errorserver_goodslist.msg}</h4>
                      </Card>
                    </Col>
                  </Row>
                :

                  <div>
                    <Card  type="inner" bordered={false}>
                      <Row>
                        <Col span={12}>
                          <FormItem label={strings.SEARCH_GOODS_PLACEHOLDER}  >
                             <Search
                               onChange={this.onSearch}
                               style={{ width: '100%' }}
                               disabled={!this.props.goods_success.status}
                               placeholder={strings.SEARCH_GOODS_PLACEHOLDER}
                             />
                          </FormItem>
                        </Col>
                        <Col offset={1} span={11}>
                          <FormItem label={strings.CATEGORY}  >
                            <TreeSelect
                               style={{width: "100%"}}
                               showSearch
                               dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
                               value={this.state.categoryValue ? this.state.categoryValue : strings.ALL_CATEGORY_LABEL}
                               treeData={this.state.categoryList}
                               onChange={this.categoryFiler}
                               treeDefaultExpandAll
                            />
                          </FormItem>
                        </Col>
                      </Row>
                    </Card>
                    <Table
                      locale={locale}
                      columns={columns}
                      pagination={{position: "top",total:this.state.searchInput.length > 0 || (this.state.categoryValue !== strings.ALL_CATEGORY_LABEL && this.state.categoryValue !== undefined) ? this.state.searchData.length :  this.state.data.length,pageSize:4}}
                      dataSource={this.state.searchInput.length > 0 || (this.state.categoryValue !== strings.ALL_CATEGORY_LABEL && this.state.categoryValue !== undefined)? this.state.searchData : this.state.data}
                    />
                  </div>
              }
            </Spin>
        </Content>
      </Layout>
    )
  }
}
export default Goods
