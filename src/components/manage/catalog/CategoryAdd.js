import React,{Component} from 'react'
import { Layout,Form,Card,Upload,Spin,Affix,message,TreeSelect,Input,Button,Icon,Row,Col} from 'antd';
import Navleft from '../Navleft'
import {SERVER} from '../../../constants/';
import { CirclePicker } from 'react-color';
import {strings} from '../../../constants/lng';
import { NavLink } from 'react-router-dom'
const { Content } = Layout;
const FormItem = Form.Item;
class CategoryAdd extends Component{
  constructor(props){
    super(props);
    this.state = {
      categoryValue: strings.MAIN_CATEGORY_TEXT,
      categoryId: "0",
      background: '#eeeeee',
      image: '',
      name_image:'',
      categoryList: []
    }
    this.imageChange = this.imageChange.bind(this);
    this.chageParent = this.chageParent.bind(this);
  }
  handleChangeComplete = (color) => {
    this.setState({ background: color.hex });
  };
  beforeUpload(file) {
    const isJPG = file.type === 'image/jpeg';
    if (!isJPG) {
      message.error('You can only upload JPG file!');
    }
    const isLt2M = file.size / 1024 / 1024 < 2;
    if (!isLt2M) {
      message.error('Image must smaller than 2MB!');
    }
    return isJPG && isLt2M;
  }
  imageChange(info){
    const reader = new FileReader();
    reader.readAsDataURL(info.file.originFileObj);
    reader.addEventListener('load', () =>this.setState({
        image: reader.result,
        name_image: info.file.originFileObj.name,
      })
    );
  }
  componentWillMount(){
    if(!this.props.category_success.status){
      this.props.catList()
    }else{
      var data =  this.props.category_success.data.slice()
      data.unshift({key: "0",label: strings.MAIN_CATEGORY_TEXT,value: strings.MAIN_CATEGORY_TEXT,id: "0"})
      this.setState({categoryList: data})
    }
  }
  handleSubmit=(e)=>{
    e.preventDefault();
    this.props.form.validateFields(['name_cat','parent_cat','image_cat','color_cat'],(err, values,) => {
      if (!err) {
        if(this.state.background==='#eeeeee'){
            values.color_cat = '#ffffff';
        }else{
          values.color_cat = this.state.background;
        }
        values.image_cat = this.state.image;
        values.parent_cat = this.state.categoryId;
        this.props.catAdd(values,this.props.history);
      }
    })
  }
  componentDidUpdate(){
    if(this.props.errorserver_catadd.status){
      message.error(this.props.errorserver_catadd.msg);
      this.props.defaultAdd()
    }
  }
  chageParent(value,e,extra){
    if(extra.triggerNode){
      this.setState({categoryValue: value});
      this.setState({categoryId: extra.triggerNode.props.id})
    }
  }
  componentWillReceiveProps(props){
    if(props.category_success.status){
      var data =  props.category_success.data.slice()
      data.unshift({key: "0",label: strings.MAIN_CATEGORY_TEXT,value: strings.MAIN_CATEGORY_TEXT,id: "0"})
      this.setState({categoryList: data})
    }
  }
  render(){
    var display = window.innerHeight;
    var contentHeight = (display*90)/100;
    const { getFieldDecorator} = this.props.form;
    return(
      <Layout>
        <Navleft keyItem="category" sub="catalog"/>
        <Content >
          <Spin spinning={this.props.loading_catadd}>
            <Form layout='vertical' onSubmit={this.handleSubmit}>
              <Affix>
                <Card  className='titleHead'>
                  <Row type="flex" align="middle">
                    <Col span={21}>
                      <h2><NavLink to='/manage/category'><Icon className="pref-link" type="arrow-left" /></NavLink>{strings.NEW_CATEGORY_TEXT}</h2>
                    </Col>
                    <Col span={3}>
                       <Button size='large' htmlType="submit" type="primary">{strings.SAVE_TEXT}</Button>
                    </Col>
                  </Row>
                </Card>
              </Affix>
              <Card style={{minHeight: contentHeight}}>
                <Row>
                  <Col span={10}>
                    <FormItem label={strings.NAME_INPUT_CATEGORY_LABEL}>
                       {getFieldDecorator('name_cat', {
                         rules: [{
                           required: true, message: strings.REQUIRE_INPUT_NAME_CATEGORY,
                         }],
                       })(
                         <Input />
                       )}
                    </FormItem>
                  </Col>
                  <Col offset={1} span={10}>
                    <FormItem label={strings.PARENT_CATEGORY_INPUT_ADDCATEGORY}>
                       {getFieldDecorator('parent_cat',{
                         initialValue: this.state.categoryValue ? this.state.categoryValue : strings.MAIN_CATEGORY_TEXT
                       })(
                          <TreeSelect
                             showSearch
                             dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
                             treeData={this.state.categoryList}
                             onChange={this.chageParent}
                             treeDefaultExpandAll
                          />
                       )}
                    </FormItem>
                  </Col>
                </Row>
                <Row>
                  <Col span={10}>
                    <FormItem label={strings.IMAGE_INPUT}>
                          <Upload
                            name="avatar"
                            action={SERVER}
                            beforeUpload={this.beforeUpload}
                            showUploadList={false}
                            onChange={this.imageChange}
                          >
                            <Button>
                              <Icon type='upload' />
                              {this.state.image.length > 0 ?
                                strings.EDIT_TEXT
                              :
                                strings.UPLOAD_TEXT}
                            </Button>
                            <br />
                            {
                              this.state.image.length > 0 ?
                                <Card style={{marginTop: '5px'}}>
                                  <img style={{width: '65px',height: 'auto'}} src={this.state.image} alt={this.state.name_image}/>
                                </Card>
                              :
                                false
                            }
                          </Upload>
                    </FormItem>
                  </Col>
                  <Col offset={1} span={10}>
                    <FormItem label={strings.COLOR_TEXT}>
                       {getFieldDecorator('color_cat')(
                            <CirclePicker width={'100%'}
                            color={ this.state.background }
                            circleSize={25} circleSpacing={10}
                            onChangeComplete={ this.handleChangeComplete }
                            colors={["#eeeeee","#000000","#b71c1c", "#ff5722", "#ffeb3b", "#4caf50", "#2196f3", "#3f51b5", "#9c27b0", "#795548", "#009688", "#fa28ff"]} />
                       )}
                     </FormItem>
                  </Col>
                </Row>
              </Card>
            </Form>
          </Spin>
        </Content>
      </Layout>
    )
  }
}
CategoryAdd = Form.create()(CategoryAdd);
export default CategoryAdd
