import React,{Component} from 'react'
import {Card,Button,Icon,Row,Col,Form,Input} from 'antd';
const FormItem = Form.Item;
let uuid = 0;
class VariantsProduct extends Component{
  remove = (k) => {
    const { form } = this.props;
    const keys = form.getFieldValue('keys');
    form.setFieldsValue({
      keys: keys.filter(key => key !== k),
    });
  }
  add = () => {
    uuid++;
    const { form } = this.props;
    const keys = form.getFieldValue('keys');
    const nextKeys = keys.concat(uuid);
    form.setFieldsValue({
      keys: nextKeys,
    });
  }
  render(){
    const { getFieldDecorator,getFieldValue} = this.props.form;
    getFieldDecorator('keys', { initialValue: [] });
    const keys = getFieldValue('keys');
    const formItems = keys.map((k, index) => {
      return (
        <div  key={k}>
          <Card
            actions={[<Icon type="delete" />, <Icon type="edit" />, <Icon type="copy" />]}
          >
            <Row>
              <Col span={6}>
                <FormItem label="Название" >
                   {getFieldDecorator(`nameMod-${k}`,{
                     rules: [{
                       required: true, message: 'Поле «Название» обязательно для заполнения.',
                     }],
                   })(
                      <Input />
                   )}
                </FormItem>
              </Col>
              <Col offset={1} span={6}>
                <FormItem label="Штрих-код" >
                   {getFieldDecorator(`barcodeMod-${k}`)(
                      <Input />
                   )}
                </FormItem>
              </Col>
              <Col offset={1} span={6}>
                <FormItem label="SKU">
                   {getFieldDecorator(`skuMod-${k}`)(
                     <Input />
                   )}
                </FormItem>
              </Col>
            </Row>
            <Row type="flex" align="middle">
              <Col span={6}>
                <FormItem label="Себестоимость">
                   {getFieldDecorator(`costPriceMod-${k}`)(
                        <Input addonAfter={<h3>&#8376;</h3>} />
                   )}
                </FormItem>
              </Col>
              <Col span={1}style={{textAlign: "center"}} >
                <h1 style={{color: 'rgba(160, 159, 159, 1)'}}>+</h1>
              </Col>
              <Col span={6}>
                <FormItem label="Наценка">
                   {getFieldDecorator(`extrachargePriceMod-${k}`)(
                      <Input addonAfter={<h3>&#037;</h3>} />
                   )}
                </FormItem>
              </Col>
              <Col span={1} style={{textAlign: "center"}} >
                <h1  style={{color: 'rgba(160, 159, 159, 1)'}}>=</h1>
              </Col>
              <Col span={6}>
                <FormItem label="Итого">
                   {getFieldDecorator(`priceMod-${k}`)(
                      <Input addonAfter={<h3>&#8376;</h3>} />
                   )}
                </FormItem>
              </Col>
            </Row>
          </Card>
          <br />
        </div>
      )
    })
    return(
      <div>
        <Card key={0}>
          <Row>
            <Col span={6}>
              <FormItem label="Название" >
                 {getFieldDecorator('nameMod-0',{
                   rules: [{
                     required: true, message: 'Поле «Название» обязательно для заполнения.',
                   }],
                 })(
                    <Input />
                 )}
              </FormItem>
            </Col>
            <Col offset={1} span={6}>
              <FormItem label="Штрих-код" >
                 {getFieldDecorator('barcodeMod-0')(
                    <Input />
                 )}
              </FormItem>
            </Col>
            <Col offset={1} span={6}>
              <FormItem label="SKU">
                 {getFieldDecorator('skuMod-0')(
                   <Input />
                 )}
              </FormItem>
            </Col>
          </Row>
          <Row type="flex" align="middle">
            <Col span={6}>
              <FormItem label="Себестоимость">
                 {getFieldDecorator('costPriceMod-0')(
                      <Input addonAfter={<h3>&#8376;</h3>} />
                 )}
              </FormItem>
            </Col>
            <Col span={1}style={{textAlign: "center"}} >
              <h1 style={{color: 'rgba(160, 159, 159, 1)'}}>+</h1>
            </Col>
            <Col span={6}>
              <FormItem label="Наценка">
                 {getFieldDecorator('extrachargePrice')(
                    <Input addonAfter={<h3>&#037;</h3>} />
                 )}
              </FormItem>
            </Col>
            <Col span={1} style={{textAlign: "center"}} >
              <h1  style={{color: 'rgba(160, 159, 159, 1)'}}>=</h1>
            </Col>
            <Col span={6}>
              <FormItem label="Итого">
                 {getFieldDecorator('priceMod-0')(
                    <Input addonAfter={<h3>&#8376;</h3>} />
                 )}
              </FormItem>
            </Col>
          </Row>
        </Card>
        <br />
        {formItems}
        <br />
        <Button type="dashed" onClick={this.add}>
           <Icon type="plus" /> Добавить еще одну модификацию
        </Button>
      </div>
    )
  }

}
export default VariantsProduct
