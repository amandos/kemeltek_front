import React,{Component} from 'react'
import {Row,Col,Form,Input} from 'antd';
const FormItem = Form.Item;
class StandardProduct extends Component{
  constructor(props){
    super(props)
    this.sumPrice = this.sumPrice.bind(this);
    this.sumMarkup = this.sumMarkup.bind(this);
  }
  sumPrice(e){
    let cost_price = this.props.form.getFieldValue(`cost_price`);
    let markup = e.target.value;
    let price = ((parseInt(cost_price,10) * parseInt(markup,10))/100) + parseInt(cost_price,10);
    if(isNaN(price)){price = 0}
    this.props.form.setFields({
         [`price`]: {
           value: price,
         },
    });
  }
  sumMarkup(e){
    let price = e.target.value;
    let cost_price = this.props.form.getFieldValue(`cost_price`);
    let markup = ((parseInt(price,10) - parseInt(cost_price,10))/parseInt(cost_price,10))*100;
    if(isNaN(markup)){markup = 0}
    this.props.form.setFields({
         [`markup`]: {
           value: markup,
         },
    });
  }
  render(){
    const { getFieldDecorator} = this.props.form;
    return(
      <div>
        <Row>
          <Col span={10}>
            <FormItem label="Штрих-код" >
               {getFieldDecorator('barcode')(
                  <Input />
               )}
            </FormItem>
          </Col>
          <Col offset={1} span={10}>
            <FormItem label="SKU">
               {getFieldDecorator('sku')(
                 <Input />
               )}
            </FormItem>
          </Col>
        </Row>
        <Row type="flex" align="middle">
          <Col span={6}>
            <FormItem label="Себестоимость">
               {getFieldDecorator('cost_price')(
                    <Input addonAfter={<h3>&#8376;</h3>} />
               )}
            </FormItem>
          </Col>
          <Col span={1}style={{textAlign: "center"}} >
            <h1 style={{color: 'rgba(160, 159, 159, 1)'}}>+</h1>
          </Col>
          <Col span={6}>
            <FormItem label="Наценка">
               {getFieldDecorator('markup')(
                  <Input onChange={this.sumPrice} addonAfter={<h3>&#037;</h3>} />
               )}
            </FormItem>
          </Col>
          <Col span={2} style={{textAlign: "center"}} >
            <h1  style={{color: 'rgba(160, 159, 159, 1)'}}>=</h1>
          </Col>
          <Col span={6}>
            <FormItem label="Итого">
               {getFieldDecorator('price')(
                  <Input onChange={this.sumMarkup} addonAfter={<h3>&#8376;</h3>} />
               )}
            </FormItem>
          </Col>
        </Row>
      </div>
    )
  }

}
export default StandardProduct
