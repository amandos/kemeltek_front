import React,{Component} from 'react'
import { Row,Col,Card,Layout,Icon} from 'antd';
import {strings} from '../../constants/lng';
import { NavLink } from 'react-router-dom'
const { Content } = Layout;
class MenuList extends Component{
  render(){
    var display = window.innerHeight;
    var contentHeight = (display*90)/100;

    return(
      <Content>
        <div style={{backgroundColor: '#3d486e',width: '100%'}}>
          <section style={{width: '100%',padding: '0 50px',display: 'table',minHeight: contentHeight}}>
            <div style={{verticalAlign: 'middle',display:'table-cell'}}>
              <Row type="flex" justify="center" align="top">
                <Col span={17}>
                  <Row >
                    <NavLink to='/manage/goods'>
                      <Col span={6}>
                        <Card hoverable={true} bodyStyle={{backgroundColor: '#589494',textAlign: 'center'}} title={strings.STATISTICS_NAV} style={{ width: 200 }}>
                          <Icon style={{ fontSize: 74, color: '#ffffff' }} type="area-chart" />
                        </Card>
                      </Col>
                    </NavLink>
                    <NavLink to='/manage/goods'>
                      <Col span={6}>
                        <Card  hoverable={true}  bodyStyle={{backgroundColor: '#f39c12',textAlign: 'center'}} title={strings.FINANCE_NAV} style={{ width: 200 }}>
                          <Icon style={{ fontSize: 74, color: '#ffffff' }} type="pay-circle-o" />
                        </Card>
                      </Col>
                    </NavLink>
                    <NavLink to='/manage/goods'>
                      <Col span={6}>
                        <Card  hoverable={true}  bodyStyle={{backgroundColor: '#1abc9c',textAlign: 'center'}} title={strings.CATALOG_NAV} style={{ width: 200 }}>
                          <Icon style={{ fontSize: 74, color: '#ffffff' }} type="tags" />
                        </Card>
                      </Col>
                    </NavLink>
                    <NavLink to='/manage/goods'>
                      <Col span={6}>
                        <Card  hoverable={true}  bodyStyle={{backgroundColor: '#3498db',textAlign: 'center'}} title={strings.STOCK_NAV} style={{ width: 200 }}>
                          <Icon style={{ fontSize: 74, color: '#ffffff' }} type="database" />
                        </Card>
                      </Col>
                    </NavLink>
                  </Row>
                  <Row style={{marginTop: '25px'}}>
                    <NavLink to='/manage/goods'>
                      <Col span={6}>
                        <Card  hoverable={true}  bodyStyle={{backgroundColor: '#c0392b',textAlign: 'center'}} title={strings.MARKETING_NAV} style={{ width: 200 }}>
                          <Icon style={{ fontSize: 74, color: '#ffffff' }} type="pie-chart" />
                        </Card>
                      </Col>
                    </NavLink>
                    <NavLink to='/manage/goods'>
                      <Col span={6}>
                        <Card  hoverable={true}  bodyStyle={{backgroundColor: '#7578f6',textAlign: 'center'}} title={strings.ACCESS_NAV} style={{ width: 200 }}>
                          <Icon style={{ fontSize: 74, color: '#ffffff' }} type="lock" />
                        </Card>
                      </Col>
                    </NavLink>
                    <NavLink to='/manage/goods'>
                      <Col span={6}>
                        <Card  hoverable={true}  bodyStyle={{backgroundColor: '#8e44ad',textAlign: 'center'}} title={strings.SETTINGS_NAV} style={{ width: 200 }}>
                          <Icon style={{ fontSize: 74, color: '#ffffff' }} type="setting" />
                        </Card>
                      </Col>
                    </NavLink>
                  </Row>
                </Col>
              </Row>
            </div>
          </section>
        </div>
      </Content>
    )
  }
}

export default MenuList
