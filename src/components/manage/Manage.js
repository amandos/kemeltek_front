import React,{Component} from 'react'
import { Layout,Menu,Row,Col,Icon,Dropdown,Card,Breadcrumb,message} from 'antd';
import {strings} from '../../constants/lng';
import {routesBreadcrumb} from '../../constants/routesBreadcrumb';
import {reactLocalStorage} from 'reactjs-localstorage';
import { NavLink } from 'react-router-dom'
const { Header } = Layout;
class Manage extends Component{
  constructor(props){
    super(props)
    this.logOut = this.logOut.bind(this);
    this.state = {
      path: '',
      breadcrumbs: []
    }
  }
  componentWillMount(){
    if(!sessionStorage.kml_auth){
      if(reactLocalStorage.get('kml_cookie_auth')){
        this.props.signin_cookie(reactLocalStorage.get('kml_cookie_auth'),this.props.history);
      }else{
        this.props.history.push('/guest/signin');
      }
    }else{
      this.props.user_info()
    };
  }
  componentWillReceiveProps(props){
    if(props.user_app.lang){
      strings.setLanguage(props.user_app.lang);
    }else{
      if(reactLocalStorage.get('lng_user')){
        strings.setLanguage(reactLocalStorage.get('lng_user'));
      }else{
        strings.setLanguage('ru');
      }
    }
    if(props.user_app.name_company){
      var loc = [];
      var path_active = props.history.location.pathname.split("/");
      for (var i = 1; i < path_active.length; i++) {
          var path_name = strings[routesBreadcrumb[path_active[i]]];
          if(path_active[i]!=='main' && path_name!==undefined){
            if(path_active[i]==='manage'){
              loc.push(<Breadcrumb.Item key={i}><NavLink to='/manage/main' ><Icon type="home" />&nbsp;<span>{path_name}</span></NavLink></Breadcrumb.Item>);
            }else{
              loc.push(<Breadcrumb.Item key={i}><NavLink to={'/manage/'+path_active[i]} >{path_name}</NavLink></Breadcrumb.Item>);
            }
          }
      }
      this.setState({breadcrumbs: loc});
    }
  }
  componentDidUpdate(){
    if(this.props.errorserver_logout.status){
      message.error(this.props.errorserver_logout.msg);
      this.props.default();
    }

  }
  logOut(){
    this.props.log_out(this.props.history);
  }
  render(){
    const menuUser = (
      <Menu>
        <Menu.Item>
          <a >{strings.BALANCE} 5400 тг.</a>
        </Menu.Item>
        <Menu.Item >
          <a onClick={this.logOut}>{strings.LOG_OUT}</a>
        </Menu.Item>
      </Menu>
    );
    const menuHelp = (
      <Menu>
        <Menu.Item>
          <a target="_blank">{strings.INSTRUCTION}</a>
        </Menu.Item>
        <Menu.Item>
          <a target="_blank">{this.props.user_app.user_email}</a>
        </Menu.Item>
      </Menu>
    );
    return(
        <div>
          {
              this.props.errorserver_user.status ?
                <Row type="flex" justify="center" align="middle">
                  <Col style={{padding: '20px 0'}} span={8}>
                    <Card style={{textAlign: 'center'}}>
                      <h4>{this.props.errorserver_user.msg}</h4>
                    </Card>
                  </Col>
                </Row>
              :
                <Layout>
                  <Header style={{ background: '#fff',borderBottom: '1px solid #ccc'}}>
                    <div className="logo" />
                    <Row type="flex" align="middle">
                      <Col span={20}>
                        <Breadcrumb separator='>'>
                          {this.state.breadcrumbs}
                        </Breadcrumb>
                      </Col>
                      <Col span={2}>
                        <Dropdown overlay={menuUser} trigger={['click']}>
                          <a style={{textDecoration: 'none'}} className="ant-dropdown-link">
                            {this.props.user_app.name_user} <Icon type="down" />
                          </a>
                        </Dropdown>
                      </Col>
                      <Col span={2} style={{textAlign: 'right'}}>
                        <Dropdown overlay={menuHelp} trigger={['click']}>
                          <a style={{textDecoration: 'none'}} className="ant-dropdown-link">
                            {strings.HELP_LABEL} <Icon type="down" />
                          </a>
                        </Dropdown>
                      </Col>
                    </Row>
                  </Header>
                  {this.props.children}
                </Layout>
          }
        </div>
    )
  }
}

export default Manage
