import React,{Component} from 'react'
import { Row, Col,Card,Icon,Form, Input, Button, Checkbox,message } from 'antd';
import { NavLink } from 'react-router-dom'
import {reactLocalStorage} from 'reactjs-localstorage';
import {strings} from '../../constants/lng';
const FormItem = Form.Item;
class Signin extends Component{
  constructor(props){
      super(props)
      if(sessionStorage.kml_auth){
        this.props.history.push('/manage/main')
      }else{
        if(reactLocalStorage.get('kml_cookie_auth')){
          this.props.signin_cookie(reactLocalStorage.get('kml_cookie_auth'),this.props.history);
        }
      };
  }
  componentDidUpdate(){
    if(this.props.errorserver_signin.status){
      message.error(this.props.errorserver_signin.msg);
      this.props.default()
    }
  }
  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
       if (!err) {
         this.props.signin(values,this.props.history)
       }
     });
  }
  render(){
    const { getFieldDecorator } = this.props.form;
    var display = window.innerHeight;
    var contentHeight = (display*70)/100;
    return(
      <section style={{width: '100%',display: 'table',minHeight: contentHeight}}>
        <div style={{verticalAlign: 'middle',display:'table-cell'}}>
          <Row type="flex" justify="center" align="middle">
            <Col style={{padding: '20px 0'}} span={8}>
              <Card
                title={
                  this.props.loading_signin ?
                    <div style={{display: 'table-cell'}}>
                      <Icon style={{color: "rgb(250, 173, 20)",fontSize: '20px',verticalAlign: 'middle'}} type="info-circle" />
                      <span style={{verticalAlign: 'middle'}}>&#8195;{strings.CHECKING}...</span>
                    </div>
                  :
                    <div style={{display: 'table-cell'}}>
                      <Icon style={{color: "#096dd9",fontSize: '20px',verticalAlign: 'middle'}} type="info-circle" />
                      <span style={{verticalAlign: 'middle'}}>&#8195;{strings.SIGNIN_HEADER}</span>
                    </div>
                }
                style={{ width: '100%' }}
              >
              <Form onSubmit={this.handleSubmit} className="login-form">
                  <FormItem>
                    {getFieldDecorator('login', {
                      rules: [
                                         { required: true, message: strings.SIGNIN_LOGIN_REQUIRED_MESSAGE }
                                       ],
                    })(
                      <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder={strings.SIGNIN_LOGIN_LABEL} />
                    )}
                  </FormItem>
                  <FormItem>
                    {getFieldDecorator('pass', {
                      rules: [{ required: true, message: strings.SIGNIN_PASS_REQUIRED_MESSAGE }],
                    })(
                      <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder={strings.SIGNIN_PASS_LABEL} />
                    )}
                  </FormItem>
                  <FormItem>
                    {getFieldDecorator('remember', {
                      valuePropName: 'checked',
                      initialValue: true,
                    })(
                      <Checkbox>{strings.REMEMBERME_LABEL}</Checkbox>
                    )}
                  </FormItem>
                  <Button style={{width:'100%'}} type="primary" htmlType="submit">
                    {strings.SIGNIN_FORM_SUBMIT_LABEL}
                  </Button>
                </Form>
              </Card>
              <div className='form_footer_menu'>
                <p><NavLink to='/guest/signup'>{strings.SIGNIN_FORM_LINK_SIGNUP_LABEL}</NavLink></p>
                <p><NavLink to='/guest/forgot'>{strings.SIGNIN_FORM_LINK_FORGOTPASS_LABEL}</NavLink></p>
              </div>
            </Col>
          </Row>
        </div>
      </section>
    )
  }
}
Signin = Form.create()(Signin);
export default Signin
