import React,{Component} from 'react'
import { Row, Col,Card,Icon,Form, Input, Button,message } from 'antd';
import { NavLink } from 'react-router-dom'
import {strings} from '../../constants/lng';
const FormItem = Form.Item;
class ForgotPass extends Component{
  componentDidUpdate(){
    if(this.props.errorserver_forgot.status){
      message.error(this.props.errorserver_forgot.msg);
      this.props.default()
    }
  }
  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
       if (!err) {
         this.props.forgot(values)
       }
     });
  }
  render(){
    const { getFieldDecorator } = this.props.form;
    var display = window.innerHeight;
    var contentHeight = (display*70)/100;
    return(
      <section style={{width: '100%',display: 'table',minHeight: contentHeight}}>
        <div style={{verticalAlign: 'middle',display:'table-cell'}}>
          <Row type="flex" justify="center" align="middle">
            {
                this.props.success_forgot.status ?
                  <Col style={{padding: '20px 0'}} span={8}>
                    <Card
                      title={
                        <div style={{display: 'table-cell'}}>
                          <Icon style={{color: "#096dd9",fontSize: '20px',verticalAlign: 'middle'}} type="info-circle" />
                          <span style={{verticalAlign: 'middle'}}>&#8195;{strings.FORGOTPASS_HEADER}</span>
                        </div>
                      }
                    >
                      <h4>{this.props.success_forgot.msg}</h4>
                    </Card>
                    <div className='form_footer_menu'>
                      <p><NavLink to='/guest/signin'>{strings.FORGOTPASS_FORM_SIGNIN_LINK}</NavLink></p>
                    </div>
                  </Col>
                :
                  <Col style={{padding: '20px 0'}} span={8}>
                    <Card
                      title={
                        this.props.loading_forgot ?
                          <div style={{display: 'table-cell'}}>
                            <Icon style={{color: "rgb(250, 173, 20)",fontSize: '20px',verticalAlign: 'middle'}} type="info-circle" />
                            <span style={{verticalAlign: 'middle'}}>&#8195;{strings.CHECKING}...</span>
                          </div>
                        :
                          <div style={{display: 'table-cell'}}>
                            <Icon style={{color: "#096dd9",fontSize: '20px',verticalAlign: 'middle'}} type="info-circle" />
                            <span style={{verticalAlign: 'middle'}}>&#8195;{strings.FORGOTPASS_HEADER}</span>
                          </div>
                      }
                      style={{ width: '100%' }}
                    >
                    <Form onSubmit={this.handleSubmit} className="login-form">
                        <FormItem>
                           {getFieldDecorator('email', {
                             rules: [
                                      { required: true, message: strings.EMAIL_INPUT_LABEL },
                                      { type: 'email', message:  strings.EMAIL_CORRECT_MESSAGE }
                                    ],
                           })(
                             <Input  size='large' prefix={<Icon type="user" style={{ fontSize: 13 }} />} placeholder={strings.EMAIL_LABEL} />
                           )}
                        </FormItem>
                        <Button style={{width:'100%'}} type="primary" htmlType="submit">
                          {strings.FORGOTPASS_FORM_SUBMIT_LABEL}
                        </Button>
                      </Form>
                    </Card>
                    <div className='form_footer_menu'>
                      <p><NavLink to='/guest/signin'>{strings.FORGOTPASS_FORM_SIGNIN_LINK}</NavLink></p>
                    </div>
                  </Col>
            }

          </Row>
        </div>
      </section>
    )
  }
}
ForgotPass = Form.create()(ForgotPass);
export default ForgotPass
