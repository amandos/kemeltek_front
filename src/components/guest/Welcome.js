import React,{Component} from 'react'
import { Row, Col,Card,Steps,Select,Input,Form,Button,Spin,message } from 'antd';
import {reactLocalStorage} from 'reactjs-localstorage';
import { NavLink } from 'react-router-dom'
import {strings} from '../../constants/lng';
const Step = Steps.Step;
const Option = Select.Option;
const FormItem = Form.Item;
var moment = require('moment-timezone');
class Welcome extends Component{
  constructor(props) {
   super(props);
   this.state = {
     current: 0,
     lang:'',
     pass:'',
     cities: [],
     currency: ''
   };
   this.next = this.next.bind(this);
   this.prev = this.prev.bind(this);
   this.changeCountry = this.changeCountry.bind(this);
   if(reactLocalStorage.get('lng_user')){
     strings.setLanguage(reactLocalStorage.get('lng_user'));
   }else{
     strings.setLanguage('ru');
   }
  }
  componentDidMount(){
    this.props.check(this.props.match.params.code)
  }
  componentDidUpdate(){
    if(this.props.errorserver.status){
      message.error(this.props.errorserver.msg);
      this.props.default()
    }
  }
  next() {
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        const current = this.state.current + 1;
        this.setState({ current });
        if(this.state.current === 0){
          this.setState({ lang:  values});
          strings.setLanguage(values.lang);
        }else if (this.state.current ===1) {
          this.setState({ pass:  values});
        }else if (this.state.current ===2) {
          this.setState({ country:  values});
        }else {
          console.log(values);
        }
      }
    });
  }
  prev() {
   const current = this.state.current - 1;
   this.setState({ current });
  }
  checkConfirm = (rule, value, callback) => {
      const form = this.props.form;
      if (value && this.state.confirmDirty) {
        form.validateFields(['confirm'], { force: true });
      }
      callback();
    }
  handleConfirmBlur = (e) => {
    const value = e.target.value;
    this.setState({ confirmDirty: this.state.confirmDirty || !!value });
  }
  checkPassword = (rule, value, callback) => {
      const form = this.props.form;
      if (value && value !== form.getFieldValue('password')) {
        callback(strings.NEWPASS_CHECK_PASS);
      } else {
        callback();
      }
  }
  handleSubmit = () => {
     this.props.form.validateFieldsAndScroll((err, values) => {
       if (!err) {
         var tz = moment.tz.guess();
         values.code = this.props.match.params.code;
         values.lang = this.state.lang.lang;
         values.pass = this.state.pass.password;
         values.currency = this.state.currency;
         values.time_zone = tz;
         this.props.welcome(values,this.props.history);
       }
     });
  }
  changeCountry = (value,props) => {
    const cities = [];
    props.props.obj.cities.map(function(item,index){
      cities.push(<Option  value={item.value} key={index}>{item.name}</Option>)
      return cities;
    });
    this.setState({cities: cities});
    this.setState({currency: {name: props.props.obj.currency.name,symbol:  props.props.obj.currency.symbol}});
  }
  countryList = () => {
    const country = [];
    strings.COUNTRY_LIST.map(function(item,index){
      country.push(<Option obj={item} value={item.value} key={index}>{item.name}</Option>)
      return country;
    });

    return country
  }
  render(){
    const { current } = this.state;
    const { cities } = this.state;
    var display = window.innerHeight;
    const { getFieldDecorator } = this.props.form;
    const country = this.countryList();
    return(
      <Spin spinning={this.props.loading || this.props.loading_check}>
        <section style={{width: '100%',display: 'table',minHeight: display}}>
          <div style={{verticalAlign: 'middle',display:'table-cell'}}>
            <Row type="flex" justify="center" align="middle">
              {
                this.props.success_check ?
                  <Col style={{padding: '20px 0'}} span={8}>
                    <Card style={{ width: '100%' }}
                      title={
                        <Steps progressDot current={current}>
                          <Step/>
                          <Step />
                          <Step />
                        </Steps>
                      }
                    >
                      {
                        current===0 ?
                          <div>
                            <h2 style={{textAlign: 'center'}}>Привет</h2>
                            <br />
                            <FormItem label={strings.SELECT_LANGUAGE}>
                              {getFieldDecorator('lang', {
                                rules: [
                                   { required: true, message: strings.SELECT_LANGUAGE }
                                 ],
                              })(
                                <Select style={{width: '100%'}} placeholder={strings.SELECT_LANGUAGE}>
                                  <Option value="kz">Qazaq</Option>
                                  <Option value="ru">Русский</Option>
                                  <Option value="en">English</Option>
                                </Select>
                              )}
                            </FormItem>
                          </div>
                        :
                          current===1 ?
                            <div>
                              <h2 style={{textAlign: 'center'}}>{strings.INVENT_PASSWORD}</h2>
                              <br />
                              <FormItem  label={strings.INPUT_PASSWORD_LABEL}>
                                   {getFieldDecorator('password', {
                                     rules: [
                                              {required: true, message: strings.ERROR_ENTER_PASSWORD},
                                              {validator: this.checkConfirm},
                                              {min: 6, message: strings.ERROR_ENTER_PASSWORD_WORDS}
                                            ],
                                   })(
                                     <Input type='password' placeholder={strings.INVENT_PASSWORD} />
                                   )}
                              </FormItem>
                              <FormItem label={strings.REPEAT_PASSWORD_LABEL}>
                                 {getFieldDecorator('confirm', {
                                   rules: [
                                            {required: true, message: strings.ENTER_REPEAT_PASSWORD_MESSAGE},
                                            {validator: this.checkPassword}
                                          ],
                                 })(
                                   <Input type='password' onBlur={this.handleConfirmBlur} placeholder={strings.REPEAT_PASSWORD_LABEL} />
                                 )}
                              </FormItem>
                            </div>
                          :
                          <Row>
                            <h2 style={{textAlign: 'center'}}>{strings.SELECT_COUNTRY_HEADER}</h2>
                            <br />
                            <FormItem label={strings.SELECT_COUNTRY_LABEL}>
                              {getFieldDecorator('country', {

                                rules: [
                                   { required: true, message: strings.ENTER_SELECT_COUNTRY_MESSAGE }
                                 ],
                              })(
                                <Select
                                  showSearch
                                  filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                                  optionFilterProp="children"
                                  style={{width: '100%'}}
                                  onChange = {this.changeCountry}
                                >
                                  {country}
                                </Select>
                              )}
                            </FormItem>
                            <FormItem label={strings.SELECT_CITY_LABEL}>
                              {getFieldDecorator('city', {
                                rules: [
                                   { required: true, message: strings.ERROR_SELECT_CITY_MESSAGE }
                                 ],
                              })(
                                <Select
                                  showSearch
                                  filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                                  optionFilterProp="children"
                                  style={{width: '100%'}}
                                >
                                  {cities}
                                </Select>
                              )}
                            </FormItem>
                          </Row>
                      }
                      <div className="steps-action">
                        {
                          current < 3 - 1
                          &&
                          <Button style={{float: 'right'}} type="primary" onClick={() => this.next()}>Далее</Button>
                        }
                        {
                          current === 3 - 1
                          &&
                          <Button style={{float: 'right'}} type="primary" onClick={() => this.handleSubmit()}>Сохранить</Button>
                        }
                        {
                          current > 0
                          &&
                          <Button style={{float: 'left',marginLeft: 8}} onClick={() => this.prev()}>
                            Назад
                          </Button>
                        }
                      </div>
                    </Card>
                  </Col>
                :
                  this.props.loading_check ?
                    <Col style={{padding: '20px 0'}} span={8}>
                      <Card style={{textAlign: 'center'}}>
                        <h4>&#8195;{strings.CHECKING}...</h4>
                      </Card>
                    </Col>
                  :
                    <Col style={{padding: '20px 0'}} span={8}>
                      <Card style={{textAlign: 'center'}}>
                        <h4>{this.props.errorserver_check.msg}</h4>
                      </Card>
                      <div className='form_footer_menu'>
                        <p><NavLink to='/guest/signin'>{strings.FORGOTPASS_FORM_SIGNIN_LINK}</NavLink></p>
                      </div>
                    </Col>
              }

            </Row>
          </div>
        </section>
      </Spin>
    )
  }
}
Welcome = Form.create()(Welcome);
export default Welcome
