import React,{Component} from 'react'
import { Layout } from 'antd';
/*import {strings} from '../../constants/lng';
import { NavLink } from 'react-router-dom'*/
const {  Content } = Layout;
class NoMatch extends Component{
  render(){
    return(
      <Layout>
        <Content>
          <h2>No match</h2>
        </Content>
      </Layout>
    )
  }
}
export default NoMatch
