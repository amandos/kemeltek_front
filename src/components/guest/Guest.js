import React,{Component} from 'react'
import { Row, Col,Layout,Menu,Divider,Button,Spin,Select,Form} from 'antd';
import fontawesome from '@fortawesome/fontawesome'
import FontAwesomeIcon from '@fortawesome/react-fontawesome'
import brands from '@fortawesome/fontawesome-free-brands'
import faCoffee from '@fortawesome/fontawesome-free-solid/faCoffee'
import {reactLocalStorage} from 'reactjs-localstorage';
import {strings} from '../../constants/lng';
const { Header, Footer, Content } = Layout;
const Option = Select.Option;
const FormItem = Form.Item;
class Guest extends Component{
  constructor(props){
    super(props)
    this.changeLan = this.changeLan.bind(this)
  }
  changeLan(value){
    reactLocalStorage.set('lng_user',value)
    strings.setLanguage(reactLocalStorage.get('lng_user'));
    this.forceUpdate();
  }
  render(){
    if(reactLocalStorage.get('lng_user')){
      strings.setLanguage(reactLocalStorage.get('lng_user'));
    }else{
      strings.setLanguage('ru');
    }

    var display = window.innerHeight;
    var contentHeight = (display*70)/100;
    var footerHeight = (display*20)/100;
    fontawesome.library.add(brands, faCoffee);
    return(
      <Spin spinning={this.props.loading_signin || this.props.loading_signup || this.props.loading_forgot || this.props.loading_newpass || this.props.signin_cookie_loading}>
        <Layout>
          <Header style={{ background: '#fff',borderBottom: '1px solid #ccc'}}>
            <div className="logo" />
            <Menu
              theme="light"
              mode="horizontal"
              defaultSelectedKeys={['1']}
              style={{ lineHeight: '61px',borderBottom: 'none',float: 'left' }}
            >
              <Menu.Item key="1">{strings.GUEST_NAV_LIST_FEATURES}</Menu.Item>
              <Menu.Item key="2">{strings.GUEST_NAV_LIST_EQUIPMENT}</Menu.Item>
              <Menu.Item key="3">{strings.GUEST_NAV_LIST_PRICE}</Menu.Item>
              <Menu.Item key="4">{strings.GUEST_NAV_LIST_INSTAL}</Menu.Item>
            </Menu>
            <Row gutter={24} type="flex" justify="space-around" align="middle" style={{float: 'right',height: '100%'}}>
              <Col span={24}>
                <Form layout="inline" >
                  <FormItem>
                    <Button type="primary">{strings.LOGIN_NAV_LABEL}</Button>
                  </FormItem>
                  <FormItem>
                    <Select size='default' onChange={this.changeLan} defaultValue={strings.getLanguage()} >
                      <Option value="kz">QAZ</Option>
                      <Option value="ru">РУС</Option>
                      <Option value="en">ENG</Option>
                    </Select>
                  </FormItem>
                </Form>
              </Col>
            </Row>
          </Header>
          <Content style={{ padding: '0 50px',minHeight: contentHeight}}>
            {
              this.props.children.length > 0?
                this.props.children
              :
                <h2>Guest nav</h2>
            }
          </Content>
          <Divider type="horizontal" style={{height: '3px'}}/>
          <Footer style={{ minHeight: footerHeight}}>
            <Row type="flex" align="top">
              <Col span={6}>
                <h4>{strings.FOOTER_NAV_NAVIGATION}</h4>
                <Divider type="horizontal" style={{height: '1px'}}/>
                <p><a>{strings.GUEST_NAV_LIST_FEATURES}</a></p>
                <p><a>{strings.GUEST_NAV_LIST_EQUIPMENT}</a></p>
                <p><a>{strings.GUEST_NAV_LIST_PRICE}</a></p>
                <p><a>{strings.GUEST_NAV_LIST_INSTAL}</a></p>
              </Col>
              <Col offset={2} span={4}>
                <h4>{strings.FOOTER_NAV_COMMUNICATION}</h4>
                <Divider type="horizontal" style={{height: '1px'}}/>
                <p>Казахстан, г. Алматы, ул. Жандосова, д. 24, кв. 11<br/>
                  <a href="tel:+77079904871">+7 707 990-48-71</a>
                </p>
                <p>E-mail<br/>
                  <a href="mailto:info@kemeltek.kz">info@kemeltek.kz</a>
                </p>
              </Col>
              <Col offset={2} span={4}>
                <h4>{strings.FOOTER_NAV_PROGRAMS}</h4>
                <Divider type="horizontal" style={{height: '1px'}}/>
                <Button type="primary">
                  <FontAwesomeIcon icon={["fab", "google-play"]} size="1x"/>&#8195;Google play
                </Button>
              </Col>
              <Col offset={2} span={4}>
                <h4>{strings.FOOTER_NAV_JOINUS}</h4>
                <Divider type="horizontal" style={{height: '1px'}}/>
                <p><Button style={{width: '100%'}} type="primary">
                  <FontAwesomeIcon icon={["fab", "facebook-f"]} size="1x"/>&#8195;facebook/@kemeltek
                </Button></p>
                <p><Button style={{width: '100%'}} type="primary">
                  <FontAwesomeIcon icon={["fab", "youtube"]} size="1x" />&#8195;youtube/@kemeltek
                </Button></p>
                <p><Button style={{width: '100%'}} type="primary">
                  <FontAwesomeIcon icon={["fab", "instagram"]} size="1x" />&#8195;instagram/@kemeltek
                </Button></p>
              </Col>
            </Row>
            <Divider type="horizontal" style={{height: '1px'}}/>
            <div style={{textAlign: 'center'}}>
              <span>Kemeltek ©2016 Back Office</span>
            </div>
          </Footer>
        </Layout>
      </Spin>
    )
  }
}

export default Guest
