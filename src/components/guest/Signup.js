﻿import React,{Component} from 'react'
import { Row, Col,Card,Icon,Form, Input, Button,message } from 'antd';
import { NavLink } from 'react-router-dom'
import {strings} from '../../constants/lng';
const FormItem = Form.Item;
class Signup extends Component{
  componentDidUpdate(){
    if(this.props.errorserver_signup.status){
      message.error(this.props.errorserver_signup.msg);
      this.props.default();
    }
  }
  handleSubmit = (e) => {
     e.preventDefault();
     this.props.form.validateFieldsAndScroll((err, values) => {
       if (!err) {
         this.props.signup(values,this.props.history)
       }
     });
  }
  render(){
    const { getFieldDecorator } = this.props.form;
    var display = window.innerHeight;
    var contentHeight = (display*70)/100;
    return(
      <section style={{width: '100%',display: 'table',minHeight: contentHeight}}>
        <div style={{verticalAlign: 'middle',display:'table-cell'}}>
          <Row type="flex" justify="center" align="middle">
            {
              this.props.success_signup.status ?
                <Col style={{padding: '20px 0'}} span={8}>
                  <Card
                    title={
                      <div style={{display: 'table-cell'}}>
                        <Icon style={{color: "#096dd9",fontSize: '20px',verticalAlign: 'middle'}} type="info-circle" />
                        <span style={{verticalAlign: 'middle'}}>&#8195;{strings.SIGNUP_USER_HEADER}</span>
                      </div>
                    }
                  >
                    <h4>{this.props.success_signup.msg}</h4>
                  </Card>
                  <div className='form_footer_menu'>
                    <p><NavLink to='/guest/signin'>{strings.HAVE_AN_ACCOUNT_LINK}</NavLink></p>
                  </div>
                </Col>
              :
              <Col style={{padding: '20px 0'}} span={8}>
                <Card
                  title={
                    this.props.loading_signup ?
                      <div style={{display: 'table-cell'}}>
                        <Icon style={{color: "rgb(250, 173, 20)",fontSize: '20px',verticalAlign: 'middle'}} type="info-circle" />
                        <span style={{verticalAlign: 'middle'}}>&#8195;{strings.CHECKING}...</span>
                      </div>
                    :
                      <div style={{display: 'table-cell'}}>
                        <Icon style={{color: "#096dd9",fontSize: '20px',verticalAlign: 'middle'}} type="info-circle" />
                        <span style={{verticalAlign: 'middle'}}>&#8195;{strings.SIGNUP_USER_HEADER}</span>
                      </div>
                  }
                  style={{ width: '100%' }}
                >
                <Form onSubmit={this.handleSubmit} className="login-form">
                  <FormItem>
                       {getFieldDecorator('name_company', {
                         rules: [
                           {required: true, message: strings.ENTER_NAME_COMPANY_MESSAGE},
                           {min: 3,message: strings.ENTER_NAME_COMPANY_MIN_WORDS_MESSAGE},
                           {max: 18,message: strings.ENTER_NAME_COMPANY_MAX_WORDS_MESSAGE}
                         ],
                       })(
                         <Input placeholder={strings.NAME_COMPANY_LABEL} />
                       )}
                    </FormItem>
                    <FormItem>
                       {getFieldDecorator('login', {
                         rules: [
                           {required: true, message: strings.LOGIN_INPUT_LABEL},
                           {min: 3,message: strings.LOGIN_INPUT_MIN_WORDS_MESSAGE},
                           {max: 18,message: strings.LOGIN_INPUT_MAX_WORDS_MESSAGE}
                         ],
                       })(
                         <Input placeholder={strings.LOGIN_LABEL}/>
                       )}
                    </FormItem>
                    <FormItem>
                       {getFieldDecorator('name_user', {
                         rules: [
                           {required: true, message: strings.NAME_INPUT_LABEL},
                           {min: 2,message: strings.NAME_INPUT_MIN_WORDS_MESSAGE},
                           {max: 18,message: strings.NAME_INPUT_MAX_WORDS_MESSAGE}
                         ],
                       })(
                         <Input placeholder={strings.NAME_LABEL}/>
                       )}
                    </FormItem>
                    <FormItem>
                       {getFieldDecorator('user_email', {
                         rules: [
                           {required: true, message: strings.EMAIL_INPUT_LABEL},
                           {type: 'email', message: strings.EMAIL_CORRECT_MESSAGE}
                         ],
                       })(
                         <Input placeholder={strings.EMAIL_LABEL}/>
                       )}
                    </FormItem>
                    <FormItem>
                       {getFieldDecorator('phone', {
                         rules: [
                           {required: true, message: strings.PHONE_INPUT_LABEL},
                           {max: 12, message: strings.PHONE_INPUT_MAX_MESSAGE}
                         ],
                       })(
                         <Input placeholder={strings.PHONE_LABEL}/>
                       )}
                    </FormItem>
                    <FormItem>
                      <Button style={{width:'100%'}} type="primary" htmlType="submit">
                        {strings.SIGNUP_FORM_SUBMIT_LABEL}
                      </Button>
                    </FormItem>
                  </Form>
                </Card>
                <div className='form_footer_menu'>
                  <p><NavLink to='/guest/signin'>{strings.HAVE_AN_ACCOUNT_LINK}</NavLink></p>
                </div>
              </Col>
            }
          </Row>
        </div>
      </section>
    )
  }
}
Signup = Form.create()(Signup);
export default Signup
