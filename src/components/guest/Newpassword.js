import React,{Component} from 'react'
import { Row, Col,Card,Icon,Form, Input, Button,message } from 'antd';
import {strings} from '../../constants/lng';
const FormItem = Form.Item;
class Newpassword extends Component{
  constructor(props){
      super(props)
      this.state = {
        confirmDirty: false
      }
  }
  componentDidUpdate(){
    if(this.props.errorserver_newpass.status){
      message.error(this.props.errorserver_newpass.msg);
      this.props.default()
    }
  }
  handleSubmit = (e) => {
       e.preventDefault();
       this.props.form.validateFieldsAndScroll((err, values) => {
         if (!err) {
           values.code = this.props.match.params.code
           this.props.new_pass(values,this.props.history)
         }
       });
    }
  checkConfirm = (rule, value, callback) => {
      const form = this.props.form;
      if (value && this.state.confirmDirty) {
        form.validateFields(['confirm'], { force: true });
      }
      callback();
    }
  handleConfirmBlur = (e) => {
    const value = e.target.value;
    this.setState({ confirmDirty: this.state.confirmDirty || !!value });
  }
  checkPassword = (rule, value, callback) => {
      const form = this.props.form;
      if (value && value !== form.getFieldValue('password')) {
        callback(strings.NEWPASS_CHECK_PASS);
      } else {
        callback();
      }
  }
  render(){
    const { getFieldDecorator } = this.props.form;
    var display = window.innerHeight;
    var contentHeight = (display*70)/100;
    return(
      <section style={{width: '100%',display: 'table',minHeight: contentHeight}}>
        <div style={{verticalAlign: 'middle',display:'table-cell'}}>
          <Row type="flex" justify="center" align="middle">
            <Col style={{padding: '20px 0'}} span={8}>
              <Card
                title={
                  this.props.loading_newpass ?
                    <div style={{display: 'table-cell'}}>
                      <Icon style={{color: "rgb(250, 173, 20)",fontSize: '20px',verticalAlign: 'middle'}} type="info-circle" />
                      <span style={{verticalAlign: 'middle'}}>&#8195;{strings.CHECKING}...</span>
                    </div>
                  :
                    <div style={{display: 'table-cell'}}>
                      <Icon style={{color: "#096dd9",fontSize: '20px',verticalAlign: 'middle'}} type="info-circle" />
                      <span style={{verticalAlign: 'middle'}}>&#8195;{strings.NEWPASS_HEADER}</span>
                    </div>
                }
                style={{ width: '100%' }}
              >
              <Form onSubmit={this.handleSubmit} className="login-form">
                <FormItem>
                     {getFieldDecorator('password', {
                       rules: [
                                {required: true, message: strings.NEWPASS_PASS_REQUIRED_MESSAGE},
                                {validator: this.checkConfirm},
                                {min: 6, message: strings.PASS_INPUT_MIN_MESSAGE}
                              ],
                     })(
                       <Input type='password' size='large' placeholder={strings.NEWPASS_PASS_LABEL} />
                     )}
                  </FormItem>
                  <FormItem>
                     {getFieldDecorator('confirm', {
                       rules: [
                                {required: true, message: strings.PASSRELOAD_REQUIRED_MESSAGE},
                                {validator: this.checkPassword}
                              ],
                     })(
                       <Input type='password' onBlur={this.handleConfirmBlur} size='large' placeholder={strings.PASSRELOAD_LABEL} />
                     )}
                  </FormItem>
                  <Button style={{width:'100%'}} type="primary" htmlType="submit">
                    {strings.NEWPASS_FORM_SUBMIT_LABEL}
                  </Button>
                </Form>
              </Card>
            </Col>
          </Row>
        </div>
      </section>
    )
  }
}
Newpassword = Form.create()(Newpassword);
export default Newpassword
