import Manage from '../../components/manage/Manage'
import {connect} from 'react-redux'
import { withRouter } from 'react-router-dom';
import {userInfoAction} from '../../actions/manage/user_info'
import {logOutAction,stateDefault} from '../../actions/manage/log_out'
import {signinCookie} from '../../actions/guest/signin_cookie'
const mapStateToProps = (state) => {
  return {
    loading_user: state.manage.user_info.loading,
    errorserver_user: state.manage.user_info.errorserver,
    user_app: state.manage.user_info.user_app,

    loading_logout:  state.manage.remove_cookie.loading,
    errorserver_logout: state.manage.remove_cookie.errorserver,

    signin_cookie_loading: state.signin_cookie.loading,
  }
}

const mapDispatchToProps = (dispatch) => ({
  user_info: ()=>{
    dispatch(userInfoAction())
  },
  log_out: (history)=>{
    dispatch(logOutAction(history))
  },
  default: ()=>{
    dispatch(stateDefault())
  },
  signin_cookie: (cookie,history)=>{
    dispatch(signinCookie(cookie,history))
  }
})
export const ManageContainer = withRouter(connect(
 mapStateToProps,
 mapDispatchToProps
)(Manage))
