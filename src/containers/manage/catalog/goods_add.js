import {GoodAddAction,stateDefAdd} from '../../../actions/manage/catalog/good_add'
import {CatListAction} from '../../../actions/manage/catalog/category_list'
import {connect} from 'react-redux'
import { withRouter } from 'react-router-dom';
import GoodsAdd from '../../../components/manage/catalog/GoodsAdd'
const mapStateToProps = (state) => {
  return {
    loading_goodadd: state.catalog.good_add.loading,
    errorserver_goodadd: state.catalog.good_add.errorserver,
    success_goodadd: state.catalog.good_add.success,

    loading_catlist: state.catalog.category_list.loading,
    errorserver_catlist: state.catalog.category_list.errorserver,
    category_success: state.catalog.category_list.success,

    user_app: state.manage.user_info.user_app,
  }
}
const mapDispatchToProps = (dispatch) => ({
  goodsAdd: (values,history)=>{
      dispatch(GoodAddAction(values,history));
  },
  defaultAdd: ()=>{
    dispatch(stateDefAdd());
  },
  catList: ()=>{
      dispatch(CatListAction());
  }
})
export const GoodsAddCont = withRouter(connect(
 mapStateToProps,
 mapDispatchToProps
)(GoodsAdd))
