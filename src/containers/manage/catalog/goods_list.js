import {GoodsListAction} from '../../../actions/manage/catalog/goods_list'
import {CatListAction} from '../../../actions/manage/catalog/category_list'
import {GoodStausAction,stateDefStatus} from '../../../actions/manage/catalog/good_status'
import {GoodDeleteAction,stateDefDelete} from '../../../actions/manage/catalog/good_delete'
import {stateDefAdd} from '../../../actions/manage/catalog/good_add'
import {connect} from 'react-redux'
import { withRouter } from 'react-router-dom';
import Goods from '../../../components/manage/catalog/Goods'
const mapStateToProps = (state) => {
  return {
    loading_goodslist: state.catalog.goods_list.loading,
    errorserver_goodslist: state.catalog.goods_list.errorserver,
    goods_success: state.catalog.goods_list.success,

    loading_catlist: state.catalog.category_list.loading,
    errorserver_catlist: state.catalog.category_list.errorserver,
    category_success: state.catalog.category_list.success,

    loading_goodstatus: state.catalog.good_status.loading,
    errorserver_goodstatus: state.catalog.good_status.errorserver,
    success_goodstatus: state.catalog.good_status.success,

    loading_gooddelete: state.catalog.good_delete.loading,
    errorserver_gooddelete: state.catalog.good_delete.errorserver,
    success_gooddelete: state.catalog.good_delete.success,

    success_goodadd: state.catalog.good_add.success,

    user_app: state.manage.user_info.user_app,
  }
}

const mapDispatchToProps = (dispatch) => ({
  goodsList: ()=>{
      dispatch(GoodsListAction());
  },
  catList: ()=>{
      dispatch(CatListAction());
  },
  goodStatus: (goods_id,status)=>{
    dispatch(GoodStausAction(goods_id,status));
  },
  defaultStatuss: ()=>{
    dispatch(stateDefStatus());
  },
  goodsDelete: (good_id)=>{
    dispatch(GoodDeleteAction(good_id));
  },
  defaultDelete: ()=>{
    dispatch(stateDefDelete());
  },
  defaultAdd: ()=>{
    dispatch(stateDefAdd())
  }
})
export const GoodsCont = withRouter(connect(
 mapStateToProps,
 mapDispatchToProps
)(Goods))
