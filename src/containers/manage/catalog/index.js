import Category from '../../../components/manage/catalog/Category'
import CategoryAdd from '../../../components/manage/catalog/CategoryAdd'
import CategoryUpdate from '../../../components/manage/catalog/CategoryUpdate'
import {CatListAction} from '../../../actions/manage/catalog/category_list'
import {CatStausAction,stateDefStatus} from '../../../actions/manage/catalog/category_status'
import {CatDeleteAction,stateDefDelete} from '../../../actions/manage/catalog/category_delete'
import {CatAddAction,stateDefAdd} from '../../../actions/manage/catalog/category_add'
import {CatInfoAction,stateDefInfo} from '../../../actions/manage/catalog/category_info'
import {CatUpdateAction,stateDefUpdate} from '../../../actions/manage/catalog/category_update'
import {connect} from 'react-redux'
import { withRouter } from 'react-router-dom';
const mapStateToProps = (state) => {
  return {
    loading_catlist: state.catalog.category_list.loading,
    errorserver_catlist: state.catalog.category_list.errorserver,
    category_success: state.catalog.category_list.success,

    loading_catstatus: state.catalog.category_status.loading,
    errorserver_catstatus: state.catalog.category_status.errorserver,
    success_catstatus: state.catalog.category_status.success,

    loading_catdelete: state.catalog.category_delete.loading,
    errorserver_catdelete: state.catalog.category_delete.errorserver,
    success_catdelete: state.catalog.category_delete.success,

    loading_catadd: state.catalog.category_add.loading,
    errorserver_catadd: state.catalog.category_add.errorserver,
    success_catadd: state.catalog.category_add.success,

    loading_catinfo: state.catalog.category_info.loading,
    errorserver_catinfo: state.catalog.category_info.errorserver,
    success_catinfo: state.catalog.category_info.success,

    loading_catupdate: state.catalog.category_update.loading,
    errorserver_catupdate: state.catalog.category_update.errorserver,
    success_catupdate: state.catalog.category_update.success,
  }
}

const mapDispatchToProps = (dispatch) => ({
  catList: ()=>{
      dispatch(CatListAction());
  },

  catStatus: (cat_id,status)=>{
    dispatch(CatStausAction(cat_id,status));
  },
  catDelete: (cats)=>{
    dispatch(CatDeleteAction(cats));
  },
  defaultDelete: ()=>{
    dispatch(stateDefDelete());
  },
  defaultStatuss: ()=>{
    dispatch(stateDefStatus());
  },
  catAdd: (values,history)=>{
    dispatch(CatAddAction(values,history));
  },
  defaultAdd: ()=>{
    dispatch(stateDefAdd());
  },
  catInfo: (cat_id)=>{
    dispatch(CatInfoAction(cat_id));
  },
  defaultInfo: ()=>{
    dispatch(stateDefInfo());
  },
  catUpdate: (values,history)=>{
    dispatch(CatUpdateAction(values,history))
  },
  defaultUpdate: ()=>{
    dispatch(stateDefUpdate());
  },
})
export const CategoryCont = withRouter(connect(
 mapStateToProps,
 mapDispatchToProps
)(Category))
export const CategoryAddCont = withRouter(connect(
 mapStateToProps,
 mapDispatchToProps
)(CategoryAdd))
export const CategoryUpdateCont = withRouter(connect(
 mapStateToProps,
 mapDispatchToProps
)(CategoryUpdate))
