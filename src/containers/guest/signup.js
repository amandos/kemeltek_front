import {connect} from 'react-redux'
import { withRouter } from 'react-router-dom';

import Signup from '../../components/guest/Signup'
import {signupAction,stateDefault} from '../../actions/guest/signup'
const mapStateToProps = (state,ownProps) => {
    return {
      loading_signup: state.guest.signup.loading,
      errorserver_signup: state.guest.signup.errorserver,
      success_signup: state.guest.signup.success,
    }
}
const mapDispatchToProps = (dispatch) => ({
  signup: (values,history)=>{
    dispatch(signupAction(values,history))
  },
  default: ()=>{
    dispatch(stateDefault())
  }
})
export const SignupContainer = withRouter(connect(
 mapStateToProps,
 mapDispatchToProps
)(Signup))
