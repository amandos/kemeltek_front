import {connect} from 'react-redux'
import { withRouter } from 'react-router-dom';
import Signin from '../../components/guest/Signin'
import {signinAction,stateDefault} from '../../actions/guest/signin'
import {signinCookie} from '../../actions/guest/signin_cookie'
const mapStateToProps = (state,ownProps) => {
  return {
    loading_signin: state.guest.signin.loading,
    errorserver_signin: state.guest.signin.errorserver,
    success_signin: state.guest.signin.success,

    signin_cookie_loading: state.signin_cookie.loading,
  }
}
const mapDispatchToProps = (dispatch) => ({
  signin: (values,history)=>{
    dispatch(signinAction(values,history))
  },
  signin_cookie: (cookie,history)=>{
    dispatch(signinCookie(cookie,history))
  },
  default: ()=>{
    dispatch(stateDefault())
  }

})
export const SigninContainer = withRouter(connect(
 mapStateToProps,
 mapDispatchToProps
)(Signin))
