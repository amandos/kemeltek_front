import Welcome from '../../components/guest/Welcome'
import {WelcomeAction,stateDefault} from '../../actions/guest/welcome'
import {checkActivation} from '../../actions/guest/check_activation'
import {connect} from 'react-redux'
import { withRouter } from 'react-router-dom';
const mapStateToProps = (state) => {
  return {
    loading: state.welcome.loading,
    errorserver: state.welcome.errorserver,
    success_welcome: state.welcome.success,

    loading_check: state.check_activation.loading,
    errorserver_check: state.check_activation.errorserver,
    success_check: state.check_activation.success,
  }
}

const mapDispatchToProps = (dispatch) => ({
  welcome: (values,history)=>{
    dispatch(WelcomeAction(values,history))
  },
  check: (code)=>{
    dispatch(checkActivation(code))
  },
  default: ()=>{
    dispatch(stateDefault())
  }
})
export const WelcomeContainer = withRouter(connect(
 mapStateToProps,
 mapDispatchToProps
)(Welcome))
