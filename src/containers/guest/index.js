import Guest from '../../components/guest/Guest'
import {connect} from 'react-redux'
import { withRouter } from 'react-router-dom';
const mapStateToProps = (state) => {
  return {
    loading_signin: state.guest.signin.loading,
    loading_signup: state.guest.signup.loading,
    loading_forgot: state.guest.forgot_pass.loading,
    loading_newpass: state.guest.new_pass.loading,
    signin_cookie_loading: state.signin_cookie.loading
  }
}

export const GuestContainer = withRouter(connect(
 mapStateToProps,
)(Guest))
