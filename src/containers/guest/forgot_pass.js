import {connect} from 'react-redux'
import { withRouter } from 'react-router-dom';
import ForgotPass from '../../components/guest/ForgotPass'
import {forgotAction,stateDefault} from '../../actions/guest/forgot_pass'
const mapStateToProps = (state,ownProps) => {
  return {
    loading_forgot: state.guest.forgot_pass.loading,
    errorserver_forgot: state.guest.forgot_pass.errorserver,
    success_forgot: state.guest.forgot_pass.success,
  }
}

const mapDispatchToProps = (dispatch) => ({
  forgot: (values)=>{
    dispatch(forgotAction(values))
  },
  default: ()=>{
    dispatch(stateDefault())
  }
})
export const ForgotContainer = withRouter(connect(
 mapStateToProps,
 mapDispatchToProps
)(ForgotPass))
