import {connect} from 'react-redux'
import { withRouter } from 'react-router-dom';
import Newpassword from '../../components/guest/Newpassword'
import {newPassAction,stateDefault} from '../../actions/guest/new_pass'
const mapStateToProps = (state,ownProps) => {
  return {
    loading_newpass: state.guest.new_pass.loading,
    errorserver_newpass: state.guest.new_pass.errorserver,
    success_newpass: state.guest.new_pass.success,
  }
}
const mapDispatchToProps = (dispatch) => ({
  new_pass: (values,history)=>{
    dispatch(newPassAction(values,history))
  },
  default: ()=>{
    dispatch(stateDefault())
  }
})
export const NewpassContainer = withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(Newpassword))
