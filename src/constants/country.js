export const country_list_ru =[
  {
    value: 'KZ',
    name: 'Казахстан',
    cities: [
      {name: 'Алматы',value: 'Almaty'},
      {name: 'Астана',value: 'Astana'},
      {name: 'Талдыкорган',value: 'Taldykorgan'},
      {name: 'Кокшетау',value: 'Kokšetau'},
      {name: 'Степногорск',value: 'Stepnogorsk'},
      {name: 'Актобе',value: 'Aktobe'},
      {name: 'Атырау',value: 'Atyrau'},
      {name: 'Усть-Каменогорск',value: 'Ustʹ-Kamenogorsk'},
      {name: 'Тараз',value: 'Taraz'},
      {name: 'Уральск',value: 'Uralʹsk'},
      {name: 'Аксай',value: 'Aksaj'},
      {name: 'Караганда',value: 'Karaganda'},
      {name: 'Жезказган',value: 'Žezkazgan'},
      {name: 'Балхаш',value: 'Balhaš'},
      {name: 'Темиртау',value: 'Temirtau'},
      {name: 'Костанай',value: 'Kostanaj'},
      {name: 'Рудный',value: 'Rudnyj'},
      {name: 'Кызылорда',value: 'Kyzylorda'},
      {name: 'Актау',value: 'Aktau'},
      {name: 'Жанаозен',value: 'Žanaozen'},
      {name: 'Павлодар',value: 'Pavlodar'},
      {name: 'Экибастуз',value: 'Èkibastuz'},
      {name: 'Петропавловск',value: 'Petropavlovsk'},
      {name: 'Шымкент',value: 'Šymkent'},
      {name: 'Туркестан',value: 'Turkestan'},
      {name: 'Семей',value: 'Semej'},
      {name: 'Риддер',value: 'Ridder'},
    ],
    currency: {
      code: 'KZT',
      name: 'Тенге',
      symbol: '&#8376;'
    }
  }
]
export const country_list_en =[
  {
    value: 'KZ',
    name: 'Kazahstan',
    cities: [
      {name: 'Almaty',value: 'Almaty'},
      {name: 'Astana',value: 'Astana'},
      {name: 'Taldykorgan',value: 'Taldykorgan'},
      {name: 'Kokšetau',value: 'Kokšetau'},
      {name: 'Stepnogorsk',value: 'Stepnogorsk'},
      {name: 'Aktobe',value: 'Aktobe'},
      {name: 'Atyrau',value: 'Atyrau'},
      {name: 'Ustʹ-Kamenogorsk',value: 'Ustʹ-Kamenogorsk'},
      {name: 'Taraz',value: 'Taraz'},
      {name: 'Uralʹsk',value: 'Uralʹsk'},
      {name: 'Aksaj',value: 'Aksaj'},
      {name: 'Karaganda',value: 'Karaganda'},
      {name: 'Žezkazgan',value: 'Žezkazgan'},
      {name: 'Balhaš',value: 'Balhaš'},
      {name: 'Temirtau',value: 'Temirtau'},
      {name: 'Kostanaj',value: 'Kostanaj'},
      {name: 'Rudnyj',value: 'Rudnyj'},
      {name: 'Kyzylorda',value: 'Kyzylorda'},
      {name: 'Aktau',value: 'Aktau'},
      {name: 'Žanaozen',value: 'Žanaozen'},
      {name: 'Pavlodar',value: 'Pavlodar'},
      {name: 'Èkibastuz',value: 'Èkibastuz'},
      {name: 'Petropavlovsk',value: 'Petropavlovsk'},
      {name: 'Šymkent',value: 'Šymkent'},
      {name: 'Turkestan',value: 'Turkestan'},
      {name: 'Semej',value: 'Semej'},
      {name: 'Ridder',value: 'Ridder'},
    ],
    currency: {
      code: 'KZT',
      name: 'Tenge',
      symbol: '&#8376;'
    }
  }
]
export const country_list_kz =[
  {
    value: 'KZ',
    name: 'Kazahstan',
    cities: [
      {name: 'Almaty',value: 'Almaty'},
      {name: 'Astana',value: 'Astana'},
      {name: 'Taldykorgan',value: 'Taldykorgan'},
      {name: 'Kokšetau',value: 'Kokšetau'},
      {name: 'Stepnogorsk',value: 'Stepnogorsk'},
      {name: 'Aktobe',value: 'Aktobe'},
      {name: 'Atyrau',value: 'Atyrau'},
      {name: 'Ustʹ-Kamenogorsk',value: 'Ustʹ-Kamenogorsk'},
      {name: 'Taraz',value: 'Taraz'},
      {name: 'Uralʹsk',value: 'Uralʹsk'},
      {name: 'Aksaj',value: 'Aksaj'},
      {name: 'Karaganda',value: 'Karaganda'},
      {name: 'Žezkazgan',value: 'Žezkazgan'},
      {name: 'Balhaš',value: 'Balhaš'},
      {name: 'Temirtau',value: 'Temirtau'},
      {name: 'Kostanaj',value: 'Kostanaj'},
      {name: 'Rudnyj',value: 'Rudnyj'},
      {name: 'Kyzylorda',value: 'Kyzylorda'},
      {name: 'Aktau',value: 'Aktau'},
      {name: 'Žanaozen',value: 'Žanaozen'},
      {name: 'Pavlodar',value: 'Pavlodar'},
      {name: 'Èkibastuz',value: 'Èkibastuz'},
      {name: 'Petropavlovsk',value: 'Petropavlovsk'},
      {name: 'Šymkent',value: 'Šymkent'},
      {name: 'Turkestan',value: 'Turkestan'},
      {name: 'Semej',value: 'Semej'},
      {name: 'Ridder',value: 'Ridder'},
    ],
    currency: {
      code: 'KZT',
      name: 'Tenge',
      symbol: '&#8376;'
    }
  }
]
