export const columns = [
  {
  title: 'Товар',
  dataIndex: 'nameGoods',
  key: 'nameGoods',
  width: '20%',
  }, {
  title: 'Категория',
  dataIndex: 'category',
  key: 'category',
  width: '20%',
  }, {
  title: 'SKU',
  dataIndex: 'sku',
  key: 'sku',
  width: '12%',
  },{
  title: 'Себест.',
  dataIndex: 'costPrice',
  key: 'costPrice',
  width: '12%',
  },{
  title: 'Цена',
  dataIndex: 'price',
  key: 'price',
  width: '12%',
  },{
  title: 'Наценка',
  dataIndex: 'markup',
  key: 'markup',
  width: '12%',
  },{
  title: '',
  dataIndex: 'edit',
  width: '5%',
  },{
  title: '',
  dataIndex: 'detail',
  width: '7%',
}
]
