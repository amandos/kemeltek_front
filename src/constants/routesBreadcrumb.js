export const routesBreadcrumb =
{
    manage: 'MAIN_NAV',
    main: 'MAIN_NAV',
    goods: 'CATALOG_NAV',
    category: 'CATEGORY_NAV',
    'category-add': 'CATEGORY_ADD_NAV',
    'category-edit': 'UPDATE_CATEGORY_NAV',
    'goods-add': 'GOODS_ADD_NAV'
}
